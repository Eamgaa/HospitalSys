/**
 * Created by .
 * User: batbold
 * Date: 5/11/11
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */
jQuery.fn.forceCostAmount =
    function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (
            key == 8 ||
            key == 9 ||
            key == 46 ||
            (key >= 37 && key <= 40) ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105));
        }).keyup(function (e) {
            if ($(this).val().length > 0) {
                $(this).val(getDecimalPay($(this).val()));
            }
        });
    };

function getDecimalPay(numval) {
    numval = numval + "";
    var cde = 1, vv = "";
    for (var i = 0; i < numval.length; i++) {
        if (numval.charAt(i) != "'") vv += numval.charAt(i);
    }
    numval = vv;
    vv = "";

    for (i = numval.length - 1; i >= 0; i--) {
        if (i != 0 && cde == 3) {
            vv += numval.charAt(i) + "'";
            cde = 1
        } else {
            vv += numval.charAt(i);
            cde++;
        }
    }
    numval = "";
    for (var i = vv.length - 1; i >= 0; i--) {
        numval += vv.charAt(i);
    }
    return numval;
}