package controllers;

import models.BedTreatmentInspection;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class BedTreatmentInspections extends Controller {
    public static void blank(Long id) {
        BedTreatmentInspection object;
        if (id != null) {
            object = BedTreatmentInspection.findById(id);
        } else {
            object = new BedTreatmentInspection();
        }
        render(object);
    }


}
