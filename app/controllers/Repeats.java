package controllers;

import models.Hospital;
import models.Repeat;
import models.User;
import play.data.binding.Binder;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by mungu on 2018-08-01.
 */
@With(Secure.class)
public class Repeats extends CRUD {
    public static void list() {
        User us = Users.getUser();
        Hospital hospital=us.hospital;
        notFoundIfNull(hospital);
        List<Repeat> objects = Repeat.find("hospital_id=?1", hospital.id).fetch();
        try {
            render(objects, hospital);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", objects, hospital);
        }
    }

    public static void blank() throws Exception {
        User us = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Repeat object = (Repeat) constructor.newInstance();
        Hospital hospital = us.hospital;

        try {
            render(type, object, hospital);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, hospital);
        }
    }


    public static void create() throws Exception {
        User us = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Repeat object = (Repeat) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        Hospital hospital = us.hospital;
        object.hospital = hospital;

        validation.valid(object);

        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            blank();
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list();
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank();
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void save(String id) throws Exception {
        User us = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Repeat object = Repeat.findById(Long.parseLong(id));
        Hospital hospital = us.hospital;
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.hospital=hospital;

        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            list();
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void delete(Long id) throws Exception {
        notFoundIfNull(id);
        Repeat object = Repeat.findById(id);
        try {
            object._delete();
        } catch (Exception e) {
            flash.error(play.i18n.Messages.get("crud.delete.error", object));
            redirect(request.controller + ".show", object._key());
        }
        flash.success(play.i18n.Messages.get("crud.deleted", object));
        list();
    }
}
