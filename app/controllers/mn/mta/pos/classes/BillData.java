package controllers.mn.mta.pos.classes;

public class BillData {
	
    public String amount;
    
    public String vat;
    
    public String cashAmount;
    
    public String nonCashAmount;
    
    public BillDetail[] stocks;
    
    public String cityTax;

    public BillBankTransaction[] bankTransactions;

    public String districtCode;

    public String posNo;

    public String customerNo;

    public String billType;

    public String billIdSuffix;

    //for Eruul mendiin daatgal
    public String billSuffixId;

    public String registerNo;

    //returned data
    public String success;
    public String billId;
    public String date;
    public String macAddress;
    public String internalCode;
    public String qrData;
    public String lottery;
    public String lotteryWarningMsg;
}
