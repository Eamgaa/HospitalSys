package controllers.mn.mta.pos.classes;

public class BillBankTransaction {

    public String rrn;

    public String bankId;

    public String bankName;
    
    public String terminalId;
    
    public String approvalCode;
    
    public String amount;
    
    public String acquiringBankId;
}
