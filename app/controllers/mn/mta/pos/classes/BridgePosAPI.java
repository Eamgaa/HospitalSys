package controllers.mn.mta.pos.classes;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.logging.*;

public class BridgePosAPI extends IoHandlerAdapter
{
	static{
		System.load("/home/user/Downloads/0000038_001_4831389/x64/libPosAPI.so");
		System.loadLibrary("BridgePosAPI");
	}
    @Override
    public void exceptionCaught( IoSession session, Throwable cause ) throws Exception
    {
        cause.printStackTrace();
    }
    
    public native String sendData();
    public native String getInformation();
    
    @Override
    public void messageReceived( IoSession session, Object message ) throws Exception
    {
    	String retValue = "start";
    	byte [] inputString = message.toString().getBytes();
    	
        String input_str =  new String(inputString);
        System.out.println("input: " + input_str);
        String [] input_str_arr = input_str.split("\\|");
//        if(input_str_arr[0].equals("sendFunction"))
//        {
        	System.out.println("load: " + input_str_arr[1]);
        	retValue = getInformation();
//        }

		System.out.println("return " + retValue);
		session.write(retValue);
		
        session.close(true);
    }
    
    @Override
    public void sessionIdle( IoSession session, IdleStatus status ) throws Exception
    {
        System.out.println( "IDLE " + session.getIdleCount( status ));
    }
}
	