package controllers.mn.mta.pos.classes;

public class Information {

    public String registerNo;

    public String branchNo;

    public String posId;

    public String dbDirPath;

    public InfoDetail[] extraInfo;
}
