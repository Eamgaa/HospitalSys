package controllers.mn.mta.pos.classes;

public class BillDetail {

    public String code;

    public String name;
    
    public String measureUnit;
    
    public String qty;
    
    public String unitPrice;
    
    public String totalAmount;
    
    public String vat;
    
    public String barCode;
    
    public String cityTax;

}
