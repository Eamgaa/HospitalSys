package controllers.mn.mta.pos.classes;

public class Result {
	
    public String billId;
    
    public String amount;
    
    public String cashAmount;
    
    public String nonCashAmount;
    
    public String vat;
    
    public String date;
    
    public String internalCode;
    
    public String lottery;
    
    public String merchantId;
    
    public String qrData;
    
    public String cityTax;
    
    public BillBankTransaction[] bankTransactions;
    
    public BillDetail[] stocks;
    
    public String errorCode;
    
    public String message;
    
    public String success;
    
    public String warningMsg;
    
    public String lotteryWarningMsg;

}
