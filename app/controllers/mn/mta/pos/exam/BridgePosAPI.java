package controllers.mn.mta.pos.exam;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;


public class BridgePosAPI extends IoHandlerAdapter
{
	static{
        System.load("/home/user/Downloads/0000038_001_4831389/x64/libPosAPI.so");
		System.loadLibrary("BridgePosAPI");
	}
    @Override
    public void exceptionCaught( IoSession session, Throwable cause ) throws Exception
    {
        cause.printStackTrace();
    }

    public native String checkAPI();                                 // PosAPI sangiin ajillagaag shalgana
    public native String getInformation();                           // haritsaj bui PosAPI sangiin medeelel harah
    public native String callFunction(String function, String data); // toReg, AA00112233 geh met shineer nemegdsen function
    public native String put(String data);                           // borluulalt, barimt zasvarlah, bagts barimt,
    public native String returnBill(String data);                    // burtgegden garsan barimtiig huchingui bolgono
    public native String sendData();                                 // NUATUS-g burtguulj tohirgoonii medeelel tatna, 72 tsagt neg burtguuleh

    @Override
    public void messageReceived( IoSession session, Object message ) throws Exception
    {
    	String retValue = "";
    	byte [] inputString = message.toString().getBytes();
    	
        String input_str =  new String(inputString);
        String [] input_str_arr = input_str.split("\\|");
        if (input_str_arr[0].equals("send")) {
            System.out.println("in from send");
        	retValue = sendData();
        	System.out.println("send return value : " + retValue);
        } else if (input_str_arr[0].equals("put")) {
        	System.out.println("put input value : " + input_str_arr[1]);
        	retValue = put(input_str_arr[1]);
        	System.out.println("put return value : " + retValue);
        } else if (input_str_arr[0].equals("getInformation")) {
            retValue = getInformation();
            System.out.println("getInformation return value : " + retValue);
        } else if (input_str_arr[0].equals("checkAPI")) {
            retValue = checkAPI();
        } else if (input_str_arr[0].equals("returnBill")) {
            retValue = returnBill(input_str_arr[1]);
        } else if (input_str_arr[0].equals("callFunction")) {
            retValue = callFunction(input_str_arr[1], input_str_arr[2]);
        }
		session.write(retValue);
        session.close(true);
    }
    
    @Override
    public void sessionIdle( IoSession session, IdleStatus status ) throws Exception
    {
        System.out.println( "IDLE " + session.getIdleCount( status ));
    }
}
	