package controllers.mn.mta.pos.exam;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import play.mvc.Controller;

public class StartBridge extends Controller {
	
	public static String bridgePosApi(int port ) {
		System.out.println("port : " + port + " start " );
		IoAcceptor acceptor = new NioSocketAcceptor();
		try {
			acceptor.setHandler(  new BridgePosAPI() );
			acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter( new TextLineCodecFactory()));
	        acceptor.bind( new InetSocketAddress(port) );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("port : " + port + " error : " + e.getMessage());
		}
		//bridgePosApi39(2000);
		return "ok: " + port;
	}
	

	
	
	public static void startBridge() {
		System.out.println(bridgePosApi(1025));
		renderText("success");
    }
}
