package controllers.mn.mta.pos.exam;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;


public class BridgePosAPI39 extends IoHandlerAdapter
{
	static{
		System.load("/home/user/Downloads/0000039_002_4831415/x64/libPosAPI.so");
		System.loadLibrary("BridgePosAPI39");
	}
    @Override
    public void exceptionCaught( IoSession session, Throwable cause ) throws Exception
    {
        cause.printStackTrace();
    }
    
    public native String sendData();
    public native String getInformation();
    public native String put(String data);
    public native String checkAPI();
    public native String returnBill(String data);
    public native String callFunction(String function, String data);

    @Override
    public void messageReceived( IoSession session, Object message ) throws Exception
    {
    	String retValue = "";
    	byte [] inputString = message.toString().getBytes();
    	
        String input_str =  new String(inputString);
        String [] input_str_arr = input_str.split("\\|");
        if (input_str_arr[0].equals("send")) {
            System.out.println("in from send 39");
            retValue = sendData();
            System.out.println("send return value : " + retValue);
        } else if (input_str_arr[0].equals("put")) {
            System.out.println("put input value : " + input_str_arr[1]);
            retValue = put(input_str_arr[1]);
            System.out.println("put return value : " + retValue);
        } else if (input_str_arr[0].equals("getInformation")) {
            retValue = getInformation();
            System.out.println("getInformation return value : " + retValue);
        } else if (input_str_arr[0].equals("checkAPI")) {
            retValue = checkAPI();
        } else if (input_str_arr[0].equals("returnBill")) {
            retValue = returnBill(input_str_arr[1]);
        } else if (input_str_arr[0].equals("callFunction")) {
            retValue = callFunction(input_str_arr[1], input_str_arr[2]);
        }

		session.write(retValue);
		
        session.close(true);
    }
    
    @Override
    public void sessionIdle( IoSession session, IdleStatus status ) throws Exception
    {
        System.out.println( "IDLE " + session.getIdleCount( status ));
    }
}
	