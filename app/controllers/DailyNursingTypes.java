package controllers;

import models.DailyNursingType;
import play.mvc.With;

import java.util.ArrayList;
import java.util.List;

@With(Secure.class)
public class DailyNursingTypes extends CRUD {
    public static void blank(Long id){
        DailyNursingType object;
        List<DailyNursingType> subTypes = new ArrayList<DailyNursingType>();
        if (id != null) {
            object = DailyNursingType.findById(id);
        } else object = new DailyNursingType();
        DailyNursingType parentType = object.parentType;
        if (parentType != null) subTypes= parentType.subTypes;
        List<DailyNursingType> typeList = DailyNursingType.find("ORDER BY id DESC").fetch();
        render(parentType, object, subTypes, typeList);
    }

    public static void create(Long id, Long parentType, String name, Integer value) {
        DailyNursingType object;
        if (id != null) {
            object = DailyNursingType.findById(id);
        } else object = new DailyNursingType();
        if (parentType != 0) {
            object.parentType = DailyNursingType.findById(parentType);
        }
        object.name = name;
        object.value = value;
        object._save();
        list();
    }

    public static void list() {
        List<DailyNursingType> objects = DailyNursingType.findAll();
        render(objects);
    }
}
