package controllers;

import models.Hospital;
import models.Room;
import models.User;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by suren on 7/16/18.
 */

@With(Secure.class)
public class Rooms extends CRUD {
    public static void list() {
        User usr = Users.getUser();
        if (!usr.isManager) forbidden();
        List<Room> objects = Room.find("hospital_id=?", usr.hospital.id).fetch();
        render(objects);
    }

    public static void blank(Long id) throws Exception {
        User usr = Users.getUser();
        if (!usr.isManager) forbidden();
        Room object;
        if (id != null) object = Room.findById(id);
        else object = new Room();
        try {
            render(object);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", object);
        }
    }

    public static void create() throws Exception {
        User usr = Users.getUser();
        if (!usr.isManager) forbidden();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Room object = (Room) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.hospital = usr.hospital;
        object._save();
        list();
    }

    public static void delete(Long id) throws Exception {
        if (!Users.getUser().isManager) forbidden();
        Room object = Room.findById(id);
        try {
            object._delete();
        } catch (Exception e) {
            flash.error(play.i18n.Messages.get("crud.delete.error", object));
            redirect(request.controller + ".show", object._key());
        }
        flash.success(play.i18n.Messages.get("crud.deleted", object));
        list();
    }
}
