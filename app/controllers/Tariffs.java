package controllers;

import models.Tariff;
import models.Hospital;
import models.User;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by suren on 7/20/18.
 */
@With(Secure.class)
public class Tariffs extends CRUD {

    public static void blank(Long id, int t_type) throws Exception {
        if (!Users.getUser().isManager) forbidden();
        Tariff object;
        if (id != null) {
            object = Tariff.findById(id);
            render(object, t_type);
        } else render(t_type);
    }

    public static void create(Long id, String description, Long price) throws Exception {
        User us = Users.getUser();
        if (!us.isManager) forbidden();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Tariff object;
        if (id == null) {
            object = (Tariff) constructor.newInstance();
        } else {
            object = Tariff.findById(id);
        }
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.hospital = us.hospital;
        object._save();
        list(object.tariffType);
    }

    public static void list(int t_type) {
        User us = Users.getUser();
        Hospital hospital = us.hospital;
        List<Tariff> tariffs = Tariff.find("hospital_id=?1 and tariffType=?2", hospital.id, t_type).fetch();
        render(tariffs, t_type);
    }

    public static void delete(Long id, Integer type) throws Exception {
        if (!Users.getUser().isManager) forbidden();
        Tariff object = Tariff.findById(id);
        try {
            object._delete();
        } catch (Exception e) {
            flash.error(play.i18n.Messages.get("crud.delete.error", object));
            redirect(request.controller + ".show", object._key());
        }
        flash.success(play.i18n.Messages.get("crud.deleted", object));
        list(type);
    }

    public static void show(Long id, Integer t_type) {
        Tariff object = Tariff.findById(id);
        render(object, t_type);
    }
}
