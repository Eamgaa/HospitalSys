package controllers;

import models.NurseExaminationAnswer;
import play.mvc.With;

import java.util.List;

/**
 * Created by suren on 9/20/18.
 */
@With(Secure.class)
public class NurseExaminationAnswers extends CRUD {
    public static void blank(Long id) {
        NurseExaminationAnswer object;
        if (id != null) {
            object = NurseExaminationAnswer.findById(id);
        } else object = new NurseExaminationAnswer();
        render(object);
    }

    public static void create(Long id, Integer category, String issue){
        NurseExaminationAnswer object;
        if (id != 0L) {
            object = NurseExaminationAnswer.findById(id);
        } else object = new NurseExaminationAnswer();
        object.category = category;
        object.issue = issue;
        object._save();
        list();
    }

    public static void list() {
        List<NurseExaminationAnswer> objects = NurseExaminationAnswer.findAll();
        render(objects);
    }
}
