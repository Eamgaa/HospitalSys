package controllers;

import com.google.gson.JsonArray;
import controllers.MyClass.DateRange;
import models.Hospital;
import oauth.signpost.http.HttpResponse;
import play.Play;
import play.libs.WS;

import java.io.File;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Functions {

    public static Character[] monCharacter = {'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'Ө', 'П', 'Р', 'С', 'Т', 'У', 'Ү', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'};

    public static Date convertHourNull(Date date) {
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
        date = new Date(calendar.get(Calendar.YEAR) - 1900,
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        return date;
    }

    public static Date PrevNextDay(Date date, int prevNext) {
        Calendar origDay = Calendar.getInstance();
        origDay.setTime(date);
        Calendar nextDay = (Calendar) origDay.clone();
        if (prevNext > 0) nextDay.add(Calendar.DAY_OF_YEAR, 1);
        else nextDay.add(Calendar.DAY_OF_YEAR, -1);
        return nextDay.getTime();
    }

    public static float durationMinute(Date dateS, Date dateF) {
        long diff = dateF.getTime() - dateS.getTime();
        return (float) diff / (60 * 1000);
    }

    public static String getSha1String(String pass) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] sha1hash = new byte[40];
            md.update(pass.getBytes("iso-8859-1"), 0, pass.length());
            sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int randomNumber(int max) {
        Random generator = new Random();
        max--;
        int randomPic = generator.nextInt(max);
        randomPic++;
        return randomPic;
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String cleanUrl(String url) {
        return url.replace("//", "/").replace("///", "/");
    }

    public static void deleteFileSingle(String fileDir) {
        try {
            File file = new File(Play.applicationPath.getAbsolutePath() + fileDir);
            if (file.exists()) file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String randomPassword() {
        Random generator = new Random();
        int random;
        String value = "";
        for (int i = 0; i < 6; i++) {
            random = generator.nextInt(10);
            value += random + "";
        }
        return value;
    }

    public static String getDecimal(Long val) {
        int c = 1;
        String vv = "", value = val.toString();
        for (int i = value.length() - 1; i >= 0; i--) {
            if (i != 0 && c == 3) {
                vv += value.charAt(i) + "'";
                c = 1;
            } else {
                vv += value.charAt(i);
                c++;
            }
        }
        value = "";
        for (int i = vv.length() - 1; i >= 0; i--) {
            value += vv.charAt(i);
        }
        return value;
    }

    public static Long getFloattoInt(Float floatVar) {
        DecimalFormat format = new DecimalFormat("#");
        if (floatVar == Math.floor(floatVar)) {
            return floatVar.longValue();
        } else
            return Long.parseLong(format.format(floatVar));
    }

    //2 огнооны хооронд хугацаа бодох
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInDays = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInDays, TimeUnit.MILLISECONDS);
    }

    public static DateRange getDateRange(String range) {
        DateRange dateRange = new DateRange();
        String[] ran = range.split("-");
        SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            dateRange.startDate = myDateFormat.parse(ran[0]);
            dateRange.finishDate = myDateFormat.parse(ran[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateRange;
    }

    public static List<Date> dateToDate(Date startDate, Date endDate) {
        List<Date> dates = new ArrayList<Date>();
        Date iDate = startDate;
        Calendar c = Calendar.getInstance();
        c.setTime(iDate);
        iDate = c.getTime();
        while (endDate.after(iDate)) {
            dates.add(iDate);
            c.setTime(iDate);
            c.add(Calendar.DATE, 1);
            iDate = c.getTime();
        }
        dates.add(iDate);
        return dates;
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        date = cal.getTime();
        return date;
    }

    public static String floatToString(Float i) {
        String str = new java.text.DecimalFormat("#.#").format(i);
        return str;
    }

    public static String arrayToString(String[] arr) {

        if (arr == null)
            return "";
        else if (arr.length == 1)
            return arr[0];

        String str = arr[0];
        int i = 1;
        do {
            str += "," + arr[i];
            i++;
        } while (arr.length > i);

        return str;
    }
}
