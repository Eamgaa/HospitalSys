package controllers;

import org.apache.commons.io.IOUtils;
import play.Play;
import play.mvc.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;

public class FileUploader extends Controller {
    public static void uploadFileCustom(String uploadPath, String qqfilename, File qqfile, String ratio) throws Exception {
        String uploadedFileName;
        System.out.println("qqfilename = "+qqfilename);
        int rw, rh;
        rw = Integer.parseInt(ratio.split("x")[0]);
        rh = Integer.parseInt(ratio.split("x")[1]);
        boolean success = true;
        String extension = "null";
        String errorMessege = "Хуулах явцад алдаа гарлаа";
        if (request.isNew) {
            FileOutputStream moveTo = null;
            try {
                InputStream data = new FileInputStream(qqfile);
                String[] length = qqfilename.split("\\.");
                extension = length[length.length - 1];
                Date currentDate = new Date();
                qqfilename = currentDate.getTime() + "";
                uploadPath += qqfilename;
                uploadedFileName = Play.applicationPath.getAbsolutePath() + uploadPath + "." + extension;
                moveTo = new FileOutputStream(new File(uploadedFileName));
                IOUtils.copy(data, moveTo);
                moveTo.close();
                ConvertToImage convertToImage = new ConvertToImage();
                if (!convertToImage.convert(uploadPath, extension, rw, rh, "none", false)) {
                    new File(Play.applicationPath.getAbsolutePath() + uploadPath + "." + extension).delete();
                    success = false;
                }
                if (!extension.endsWith("jpg")) {
                    new File(Play.applicationPath.getAbsolutePath() + uploadPath + "." + extension).delete();
                    extension = "jpg";
                }
            } catch (Exception ex) {
                success = false;
                ex.printStackTrace();
                if (moveTo != null)
                    moveTo.close();
            }
        }
        renderText("{\"success\":" + success + ",\"filedir\":\"" + uploadPath + "\",\"filename\":\"" + qqfilename + "\",\"extension\":\"" + extension + "\""
                + (!success ? ",\"error\":\"" + errorMessege + "\"" : "") + "}");
    }

    public static String createProfilePicture(String profilePicture, int x, int y, int w, int h) {
        ConvertToImage convertToImage = new ConvertToImage();
        String path = profilePicture.substring(0, profilePicture.length() - 4);
        String ext = profilePicture.substring(profilePicture.length() - 3, profilePicture.length());
        convertToImage.convertRectEllipse(path, ext, x * 3, y * 3, w * 3, h * 3);
        if (!ext.toLowerCase().equals("png")) Functions.deleteFileSingle(profilePicture);
        return path + ".png";
    }
}
