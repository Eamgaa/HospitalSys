package controllers;

import models.*;
import play.mvc.With;

import java.util.Date;
import java.util.List;

/**
 * Created by suren on 9/13/18.
 */
@With(Secure.class)
public class UserUsedDrugs extends CRUD {
    public static void blank(Long id) {
        TreatmentPlan treatmentPLan = TreatmentPlan.findById(id);
        BedTreatment bedTreatment = treatmentPLan.bedTreatment;
        List<Date> dates = Functions.dateToDate(bedTreatment.startDate, bedTreatment.endDate);

        List<TreatmentPlan> treatmentPlans = TreatmentPlan.find("type=2 AND bedTreatment_id=?1", bedTreatment.id).fetch();
        for (TreatmentPlan tp : treatmentPlans) {
            UserUsedDrug object;
            object = UserUsedDrug.find("bedTreatment_id=?1 AND drug_id=?2", bedTreatment.id, tp.drug.id).first();

            if (object == null) {
                object = new UserUsedDrug();
                object.treatmentPlan = tp;
                object.bedTreatment = treatmentPLan.bedTreatment;
                object.drug = treatmentPLan.drug;
                Drug drug = treatmentPLan.drug.drug;
                object.drugName = drug.sellName;
                object.dose = drug.dose.dose;
                object.instruction = drug.instruction;
                object._save();
                treatmentPLan.bedTreatment.usedDrugs.add(object);
            }
        }

        List<UserUsedDrug> objects;
//        objects = UserUsedDrug.find("bedTreatment_id=?1", bedTreatment.id).fetch();
        objects = UserUsedDrug.find("treatmentPlan_id=?1", id).fetch();

        render(dates, bedTreatment, objects);
    }

    public static void save(Long id, Date date, Boolean give, Integer type) {
        UserUsedDrug object = UserUsedDrug.findById(id);
        boolean changed = false;
        for (UserUsedDrugDate d : object.dates) {
            if (d.date != null && d.date.compareTo(date) == 0) {
                if (type == 1) d.morning = give;
                if (type == 2) d.day = give;
                if (type == 3) d.evening = give;
                d._save();
                changed = true;
            }
        }

        if (!changed) {
            UserUsedDrugDate userUsedDrugDate = new UserUsedDrugDate();
            userUsedDrugDate.date = date;
            UserUsedDrug userUsedDrug = UserUsedDrug.findById(id);
            userUsedDrugDate.drug = userUsedDrug;
            if (type == 1) userUsedDrugDate.morning = give;
            if (type == 2) userUsedDrugDate.day = give;
            if (type == 3) userUsedDrugDate.evening = give;
            userUsedDrugDate._save();
            userUsedDrug.dates.add(userUsedDrugDate);
            System.out.print(userUsedDrug.dates.size());
        }
        renderText("success");
    }
}
