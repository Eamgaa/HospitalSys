package controllers;

import models.*;
import play.mvc.With;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@With( Secure.class)
public class DailyNursings extends CRUD {
    public static void blank(Long id) {
        BedTreatment bt = BedTreatment.findById(id);
        List<DailyNursingType> parents = DailyNursingType.find("parentType IS NULL").fetch();
        DailyNursingAnswer answer = DailyNursingAnswer.find("category=?1", 1).first();
        List<DailyNursingAnswer> type1 = DailyNursingAnswer.find("category=?1", 2).fetch();
        List<DailyNursingAnswer> type2 = DailyNursingAnswer.find("category=?1", 3).fetch();
        ArrayList<List<DailyNursingAnswer>> answers =new ArrayList<List<DailyNursingAnswer>>();
        answers.add(type1);
        answers.add(type2);
        List<DailyNursing> dailyNursings = bt.dailyNursings;
        render(parents, answers, answer, bt, dailyNursings);
    }

    public static void show(Long id) {
        System.out.print("\ndn_id - " + id);
        DailyNursing object = DailyNursing.findById(id);
        List<DailyNursingType> parents = DailyNursingType.find("parentType IS NULL").fetch();
        DailyNursingAnswer answer = DailyNursingAnswer.find("category=?1", 1).first();
        List<DailyNursingAnswer> type1 = DailyNursingAnswer.find("category=?1", 2).fetch();
        List<DailyNursingAnswer> type2 = DailyNursingAnswer.find("category=?1", 3).fetch();
        ArrayList<List<DailyNursingAnswer>> answers =new ArrayList<List<DailyNursingAnswer>>();
        answers.add(type1);
        answers.add(type2);
        render(object, parents, answers, answer);
    }

    public static void create (Long id, String date,  Long bt_id, Long[] questions, Long[] answers) {
        System.out.print("\nbt_id - " + bt_id);
        BedTreatment bt = BedTreatment.findById(bt_id);
        DailyNursing object;
        if (id != null && id != 0L) {
            object = DailyNursing.findById(id);
            List<DailyNursingQA> qas = DailyNursingQA.find("dailyNursing_id=?1", id).fetch();
            for (DailyNursingQA qa: qas) qa.delete();
        } else {
            object = new DailyNursing();
            object.qaList = new ArrayList<DailyNursingQA>();
        }
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try {
            object.date = dateTimeFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        object.bedTreatment = bt;
        if (questions != null) {
            for (int i = 0; i < questions.length; i++) {
                DailyNursingQA daqa = new DailyNursingQA();
                daqa.dailyNursingType = DailyNursingType.findById(questions[i]);
                daqa.dailyNursingAnswer = DailyNursingAnswer.findById(answers[i]);
                daqa.dailyNursing = object;
                object.qaList.add(daqa);
            }
        }
        object.save();
        bt.dailyNursings.add(object);
        bt.save();
        renderText("success");
    }
}
