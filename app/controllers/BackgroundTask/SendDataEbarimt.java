package controllers.BackgroundTask;

import controllers.EBarimt;
import models.Hospital;
import play.jobs.Job;
import play.jobs.On;

import java.util.List;

@On("36 16 0 * * ?") //everyday at ss mm hh
public class SendDataEbarimt extends Job {
    public void doJob(){
        List<Hospital> hospitals = Hospital.find("portNumber IS NOT NULL AND districtCode IS NOT NULL").fetch();
        System.out.print("\nall hospitals are "+hospitals.size()+"\n");
        for (Hospital h: hospitals) {
            System.out.print("\n"+h.toString()+"--on port-"+h.portNumber+"result"+EBarimt.getInformatioin(h.id));
        }
    }
}
