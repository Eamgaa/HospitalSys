package controllers;

import controllers.MyClass.DateRange;
import models.*;
import play.mvc.With;

import java.util.*;

/**
 * Created by mungu on 2018-07-25.
 */
@With(Secure.class)
public class Inspections extends CRUD {

    public static void register(Long ins_id, Long tab) {
        Date date = Functions.convertHourNull(new Date());
        if (ins_id.intValue() > 0) {
            Inspection ex = Inspection.findById(ins_id);
            if (ex == null) forbidden();
        }
        List<Inspection> inspections = regInspectionList(date, "");
        User usr = Users.getUser();
        Hospital hospital = usr.hospital;
        render(inspections, date, ins_id, tab, hospital);
    }

    public static void loadUsers(Date date) {
        List<Inspection> inspections = regInspectionList(date, "");
        Long ins_id = 0L;
        if (inspections.size() > 0)
            ins_id = inspections.get(0).id;
        render(inspections, date, ins_id);
    }

    public static List<Inspection> regInspectionList(Date date, String searchUser) {

        Date dateF = Functions.PrevNextDay(date, 1);
        User owner = Users.getUser();
        List<Inspection> inspections;

        if (owner.isRoleDoctor()) {
            inspections = Inspection.find("hospital.id=?1 and startDate>= ?2 and (doctor.id=?3 or owner.id=?3) and customer.firstName like '" + searchUser + "%'",
                    owner.hospital.id, date, owner.id).fetch();
//            inspections = Inspection.find("hospital.id=?1 and startDate>=?2 and startDate<?3 and doctor_id=?4 and customer.firstName like '" + searchUser + "%'" +
//                    " order by customer.id desc", owner.hospital.id, date, dateF, owner.id).fetch();

//            inspections = Inspection.find("hospital.id=?1 and startDate>=?2 and doctor.id=?3 and customer.firstName like '" + searchUser + "%'" +
//                    " order by customer.id desc", owner.hospital.id, date, owner.id).fetch();
//            inspections = Inspection.find("hospital.id=?1 and startDate>=?2 and startDate<?3 and doctor.id=?4 and customer.firstName like '" + searchUser + "%'" +
//                    " order by customer.id desc", owner.hospital.id, date, dateF, owner.id).fetch();
            for (Iterator<Inspection> it = inspections.iterator(); it.hasNext(); ) {
                Inspection ins = it.next();
                History history = History.find("typeId=1 AND pkid=" + ins.id).first();
                //төлбөрөө төлсөн эсэхийг нь шалгаж байна.
                if (history != null && history.payment != 0) {
//                    it.remove();
                }
            }
        } else {
            inspections = Inspection.find("hospital.id=?1 and startDate>=?2 and startDate<?3 and customer.firstName like '" + searchUser + "%'" +
                    " order by customer.id desc", owner.hospital.id, date, dateF).fetch();
        }
        return inspections;
    }

    public static void searchUser(Character registerChar1, Character registerChar2, String registerNumber, String firstName, String phone) {
        registerNumber = registerNumber.toLowerCase();
        List<User> users = User.find("(registerChar1 = ?1 AND registerChar2 = ?2 AND registerNumber = ?3) OR ( firstName LIKE ?4 AND (phone1 = ?5 OR phone2 = ?5))",
                registerChar1, registerChar2, registerNumber, "%" + firstName + "%", phone).fetch();
        render(users, registerChar1, registerChar2, registerNumber, firstName, phone);
    }

    public static Inspection addUserRegFunc(Long uid, Date date) {
        User owner = Users.getUser();

        Inspection inspection = new Inspection();
        inspection.hospital = owner.hospital;
        inspection.customer = User.findById(uid);
        inspection.owner = owner;
        if (owner.isRoleDoctor()) {
            inspection.doctor = owner;
            inspection.sector = owner.sectorUserRels.get(0).sector;
        }
        inspection.startDate = date;
        inspection.deletePlug = false;
        inspection.create();

        return inspection;
    }

    public static void addIns(Long uid, Date date) {
        Inspection inspection = addUserRegFunc(uid, date);
        renderText(inspection.id);
    }

    public static void editInspection(Long ins_id, Long sector_id, Long doctor_id, String startDate, String endDate, Long tarif_id)
            throws Exception {

        User owner = Users.getUser();

        Date sDate = Consts.dateTimeFormat.parse(startDate + ":00");
        Date eDate = Consts.dateTimeFormat.parse(endDate + ":00");

        Tariff tariff = Tariff.findById(tarif_id);
        User doctor = User.findById(doctor_id);
        Inspection inspection = Inspection.findById(ins_id);
        inspection.doctor = doctor;
        inspection.sector = Sector.findById(sector_id);
        inspection.startDate = sDate;
        inspection.endDate = eDate;
        inspection.tariff = tariff;
        inspection.totalPrice = inspection.tariff.price;
        inspection.serviceDate = new Date();
        inspection._save();

        History history = History.find("typeId = 1 AND pkid = " + ins_id).first();
        if (history == null) {
            history = new History();
            history.historyItems = new ArrayList<HistoryItem>();
            history.save();
        }
        history.user = inspection.customer;
        history.hospital = owner.hospital;
        history.typeId = 1;
        history.pkid = inspection.id;
        history.inspection = inspection;
        history.owner = owner;
        history.payment = inspection.totalPrice;
        history._save();
        HistoryItem item = new HistoryItem();
        item.history = history;
        item.tariff = tariff;
        item.code = tariff.code;
        item.name = tariff.description;
        item.measureUnit = tariff.measureUnit;
        item.unitPrice = Consts.decimalFormat.format(tariff.price);
        item.qty = "1.00";
        item.totalAmount = Consts.decimalFormat.format(tariff.price);
        item.paid = false;
        item._save();
        history._save();
        renderText(ins_id);
    }

    public static void cancelInspection(Long ins_id) {
        Inspection inspection = Inspection.findById(ins_id);
        inspection.deletePlug = true;
        inspection._save();
        renderText("success");
    }

    public static void loadSector(Long ins_id) {
        User us = Users.getUser();
        Inspection inspection = Inspection.findById(ins_id);

        User customer = null;
        List<Inspection> inspections = null;

        if (inspection != null) {
            customer = inspection.customer;
            if (inspection.hospital != us.hospital) forbidden();
            inspections = Inspection.find("customer.id = ?1 ORDER BY serviceDate DESC", customer.id).fetch();
        }

        Date nowDate = new Date();
        Date lateDate = new Date();
        lateDate.setMinutes(lateDate.getMinutes() + 30);
        List<Sector> sectors = Sector.find("hospital.id = ?1", us.hospital.id).fetch();
        List<Tariff> tariffs = Tariff.find("hospital.id = ?1 and tariffType=2", us.hospital.id).fetch();
        render(ins_id, inspection, customer, sectors, tariffs, nowDate, lateDate, inspections);
    }

    public static void loadInspection(Long ins_id) {
        User user = Users.getUser();
        List<Tariff> tariffs = Tariff.find("hospital.id = ?1 and tariffType=1", user.hospital.id).fetch();
        List<Repeat> repeats = Repeat.find("hospital.id=?1", user.hospital.id).fetch();
        List<HospitalDrug> drugs = HospitalDrug.find("hospital.id = ?1", user.hospital.id).fetch();
        List<Complaint> complaints = Complaint.find("hospital.id = ?1", user.hospital.id).fetch();
        List<HospitalDiagnose> diagnoses = user.hospital.hospitalDiagnoses;
        Inspection inspection = Inspection.findById(ins_id);
        Date nowDate = new Date();
        List<InspectionType> inspectionTypes = InspectionType.findAll();
        render(ins_id, inspection, tariffs, repeats, drugs, complaints, diagnoses, nowDate, inspectionTypes);
    }

    public static void saveInspection(Long ins_id, String receiptNumber, Long insType_id, String complaintVal, String complaintNote, String questionnaireNote, String diagnoseVal, String note) {
        List<InspecDiagnoseRel> oldDiagnoseRels = InspecDiagnoseRel.find("inspection.id = ?1", ins_id).fetch();
        for (InspecDiagnoseRel rel : oldDiagnoseRels)
            rel._delete();
        List<InspecComplaintRel> oldComplaintRels = InspecComplaintRel.find("inspection.id = ?1", ins_id).fetch();
        for (InspecComplaintRel rel : oldComplaintRels)
            rel._delete();

        Inspection inspection = Inspection.findById(ins_id);
        inspection.receiptNumber = receiptNumber;
        inspection.note = note;
        inspection.complaintNote = complaintNote;
        inspection.questionnaireNote = questionnaireNote;
        inspection.inspectionType = InspectionType.findById(insType_id);

        String[] arrayDiag = diagnoseVal.split(",");
        for (int i = 0; i < arrayDiag.length; i++) {
            if (arrayDiag[i] != null && !arrayDiag[i].equals("null")) {
                InspecDiagnoseRel diagnoseRel = new InspecDiagnoseRel();
                diagnoseRel.diagnose = Diagnose.findById(Long.parseLong(arrayDiag[i]));
                diagnoseRel.inspection = inspection;
                inspection.inspecDiagnoseRels.add(diagnoseRel);
            }
        }
        String[] arrayComp = complaintVal.split(",");
        for (int i = 0; i < arrayComp.length; i++) {
            if (arrayComp[i] != null && !arrayComp[i].equals("null")) {
                InspecComplaintRel complaintRel = new InspecComplaintRel();
                complaintRel.complaint = Complaint.findById(Long.parseLong(arrayComp[i]));
                complaintRel.inspection = inspection;
                inspection.inspecComplaintRels.add(complaintRel);
            }
        }
        inspection._save();
        renderText("success");
    }

    public static void saveInspectionPlan(Long ins_id, String[] tariff, String[] repeat, String[] dateRange) throws Exception {
        List<InspectionPlan> inspectionPlans = InspectionPlan.find("inspection.id = ?1", ins_id).fetch();
        for (InspectionPlan rel : inspectionPlans) rel._delete();

        Long totalPrice = 0L;
        Inspection inspection = Inspection.findById(ins_id);

        History history = History.find("typeId = 2 AND pkid = " + ins_id).first();
        if (history == null) {
            history = new History();
            history.historyItems = new ArrayList<HistoryItem>();
            history._save();
        }

        for (int i = 0; i < tariff.length; i++) {
            DateRange range = Functions.getDateRange(dateRange[i]);
            InspectionPlan rel = new InspectionPlan();
            Tariff t = Tariff.findById(Long.parseLong(tariff[i]));
            rel.inspection = inspection;
            rel.tariff = t;
            rel.repeat = Repeat.findById(Long.parseLong(repeat[i]));
            rel.startDate = range.startDate;
            rel.endDate = range.finishDate;
            rel.price = rel.tariff.price * rel.countTimes(); // price * repeat * duration( days)
            rel._save();
            totalPrice += rel.price;

            HistoryItem item = new HistoryItem();
            item.history = history;
            item.tariff = t;
            item.code = t.code;
            item.name = t.description;
            item.measureUnit = t.measureUnit;
            item.unitPrice = Consts.decimalFormat.format(t.price);
            item.qty = "1.00";
            item.totalAmount = Consts.decimalFormat.format(t.price);
            item.paid = false;
            item._save();
        }

        history.user = inspection.customer;
        history.hospital = Users.getUser().hospital;
        history.typeId = 2;
        history.pkid = ins_id;
        history.payment = totalPrice;
        history.owner = Users.getUser();
        for (int i = 0; i < tariff.length; i++) {
        }
        history._save();

        renderText("success");
    }

    public static void saveInspectionDrug(Long ins_id, String[] instruction, String[] drug, String[] repeat, String[] dateRange, boolean[] countPrice, boolean[] isNew) throws Exception {
        List<InspectionDrug> inspectionDrugs = InspectionDrug.find("inspection.id = ?1", ins_id).fetch();
        for (InspectionDrug rel : inspectionDrugs)
            rel._delete();
        Inspection inspection = Inspection.findById(ins_id);
        History history = History.find("typeId = 6 AND pkid = " + ins_id).first();
        if (history == null) {
            history = new History();
            history.historyItems = new ArrayList<HistoryItem>();
            history._save();
        } else if (!inspection.isPaid()) {
            List<HistoryItem> historyItems = HistoryItem.find("history.id=?1", history.id).fetch();
            for (HistoryItem item : historyItems) {
                history.payment -= (long) Double.parseDouble(item.totalAmount);
                item._delete();
            }
        }

        Long totalPrice = 0L;

        for (int i = 0; i < drug.length; i++) {
            DateRange range = Functions.getDateRange(dateRange[i]);
            HospitalDrug hd = HospitalDrug.findById(Long.parseLong(drug[i]));
            InspectionDrug rel = new InspectionDrug();
            rel.inspection = inspection;
            rel.drug = hd;
            rel.repeat = Repeat.findById(Long.parseLong(repeat[i]));
            rel.startDate = range.startDate;
            rel.endDate = range.finishDate;
            rel.price = rel.drug.unitPrice * rel.countTimes();
            rel.instruction = instruction[i];
            rel.shouldPay = countPrice[i];
            rel._save();

            if (!isNew[i] && countPrice[i]) {
                HistoryItem item = new HistoryItem();
                item.history = history;
                item.drug = hd;
                item.code = hd.code;
                item.name = hd.drug.toString();
                item.measureUnit = hd.measureUnit;
                item.unitPrice = Consts.decimalFormat.format(rel.drug.unitPrice);
                item.qty = Consts.decimalFormat.format(rel.countTimes());
                item.totalAmount = Consts.decimalFormat.format(hd.unitPrice * rel.countTimes());
                item.paid = inspection.isPaid();
                item._save();
                history.historyItems.add(item);
                totalPrice += rel.price;
            }
        }

        history.user = inspection.customer;
        history.hospital = Users.getUser().hospital;
        history.typeId = 6;
        history.pkid = ins_id;
        history.payment += totalPrice;
        history.owner = Users.getUser();
        history.inspection = inspection;
        history._save();
        renderText("success");
    }

    public static void drugPrint(Long ins_id) {
        Date nowDate = new Date();
        Inspection inspection = Inspection.findById(ins_id);
        render(inspection, nowDate);
    }

    public static void addNotes(Long id, String note) {
        InspectionPlan inspectionPlan = InspectionPlan.findById(id);
        NurseNote nurseNote = new NurseNote();
        nurseNote.date = new Date();
        nurseNote.note = note;
        nurseNote.user = Users.getUser();
        nurseNote.inspectionPlan = inspectionPlan;
        nurseNote._save();
        inspectionPlan.nurseNotes.add(nurseNote);
        inspectionPlan._save();
        renderText("success");
    }

    public static void notes(Long id) {
        List<NurseNote> nurseNotes = NurseNote.find("inspectionPlan_id=?1", id).fetch();
        InspectionPlan notePlan = InspectionPlan.findById(id);
        render(nurseNotes, notePlan);
    }

    public static void loadQuestion(Long ins_id, Long value) {
        User owner = Users.getUser();
        Inspection inspection = Inspection.findById(ins_id);
        Survey survey = Survey.find("value=?1", value).first();
        render(inspection, survey);
    }

    public static void saveSurvey(Long insId, long sId, String selectedAnswer, String inputAnswer,
                                  String extraSelectedAnswer, String extraInputAnswer) {

        User owner = Users.getUser();
        Inspection inspection = Inspection.findById(insId);
        Survey survey = Survey.findById(sId);

        SurveyInsUserRel surveyInsUserRel;
        if (inspection.surveyInsUserRel != null) {
            surveyInsUserRel = inspection.surveyInsUserRel;

            //when update survey answers should have delete first
            for (SurveyInsUserRelAnswer relAnswer : surveyInsUserRel.relAnswers)
                relAnswer._delete();

        } else {
            surveyInsUserRel = new SurveyInsUserRel();
            surveyInsUserRel.survey = survey;
            surveyInsUserRel.inspection = inspection;
            surveyInsUserRel.owner = owner;
            surveyInsUserRel.createdDate = new Date();
            surveyInsUserRel.create();

            inspection.surveyInsUserRel = surveyInsUserRel;
            inspection.save();
        }

        // эхний асуулт - сонгодог хариулттай
        if (selectedAnswer != null && selectedAnswer.length() > 0) {
            String[] answer = selectedAnswer.split(":");
            for (int i = 0; i < answer.length; i++) {
                String[] arr = answer[i].split("\\|");
                if (arr.length > 0) {
                    SurveyInsUserRelAnswer relAnswer = SurveyInsUserRelAnswer.
                            find("surveyInsUserRel.id = ?1 AND questionRel.id = ?2",
                                    surveyInsUserRel.id, Long.valueOf(arr[0])).first();
                    if (relAnswer == null) {
                        relAnswer = new SurveyInsUserRelAnswer();
                        relAnswer.questionRel = SurveyQuestionRel.findById(Long.valueOf(arr[0]));
                        relAnswer.surveyInsUserRel = surveyInsUserRel;
                        relAnswer.answer = arr[1];
                        relAnswer.create();
                    } else {
                        //multi selection тохиолдолд хуучин хариулт
                        // дээр | -аар тусгаарлан нэмэн бичнэ.
                        relAnswer.answer += "|" + arr[1];
                        relAnswer.save();
                    }
                }
            }
        }

        // эхний асуулт - бичдэг хариулттай
        if (inputAnswer != null && inputAnswer.length() > 0) {
            String[] answer = inputAnswer.split(":");
            for (int i = 0; i < answer.length; i++) {
                String[] arr = answer[i].split("\\|");
                if (arr.length > 1) {
                    SurveyInsUserRelAnswer relAnswer = new SurveyInsUserRelAnswer();
                    relAnswer.questionRel = SurveyQuestionRel.findById(Long.valueOf(arr[0]));
                    relAnswer.surveyInsUserRel = surveyInsUserRel;
                    relAnswer.answer = arr[1];
                    relAnswer.create();
                }
            }
        }

        // нэмэлт асуулт - сонгодог хариулттай
        if (extraSelectedAnswer != null && extraSelectedAnswer.length() > 0) {
            String[] answer = extraSelectedAnswer.split(":");
            for (int i = 0; i < answer.length; i++) {
                String[] arr = answer[i].split("\\|");
                if (arr.length > 0) {
                    SurveyInsUserRelAnswer relAnswer = SurveyInsUserRelAnswer.
                            find("surveyInsUserRel.id = ?1 AND questionRel.id = ?2",
                                    surveyInsUserRel.id, Long.valueOf(arr[0])).first();
                    relAnswer.extraAnswer = relAnswer.extraAnswer == null
                            ? arr[1] : relAnswer.extraAnswer + "|" + arr[1];
                    relAnswer.save();
                }
            }
        }

        // нэмэлт асуулт - бичдэг хариулттай
        if (extraInputAnswer != null && extraInputAnswer.length() > 0) {
            String[] answer = extraInputAnswer.split(":");
            for (int i = 0; i < answer.length; i++) {
                String[] arr = answer[i].split("\\|");
                if (arr.length > 1) {
                    SurveyInsUserRelAnswer relAnswer = SurveyInsUserRelAnswer.
                            find("surveyInsUserRel.id = ?1 AND questionRel.id = ?2",
                                    surveyInsUserRel.id, Long.valueOf(arr[0])).first();
                    relAnswer.extraAnswer = arr[1];
                    relAnswer.save();
                }
            }
        }

        renderText("success");
    }

    public static void printInspection(Long insId) {
        Date nowDate = new Date();
        Inspection inspection = Inspection.findById(insId);
        render(inspection, nowDate);
    }
}
