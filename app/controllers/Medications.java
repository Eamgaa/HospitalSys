package controllers;

import models.MedicationDate;
import models.TreatmentPlan;
import play.mvc.With;

import java.util.Date;
import java.util.List;

/**
 * Created by user on 1/3/2019.
 */
@With(Secure.class)
public class Medications extends CRUD {

    public static void blank(long id) {

        TreatmentPlan treatmentPlan = TreatmentPlan.findById(id);
        List<Date> dates = Functions.dateToDate(treatmentPlan.bedTreatment.startDate, treatmentPlan.bedTreatment.endDate);
        List<TreatmentPlan> treatmentPlans = TreatmentPlan.find("type=2 AND bedTreatment_id=?1", treatmentPlan.bedTreatment.id).fetch();
//        List<TreatmentPlan> treatmentPlans = TreatmentPlan.find("type=2 AND id=?1", id).fetch();
        render(dates, treatmentPlans);
    }

    public static void save(Long id, Date date, Boolean give, Integer type) {
        TreatmentPlan treatmentPlan = TreatmentPlan.findById(id);
        boolean changed = false;
        for (MedicationDate d : treatmentPlan.medicationDates) {
            if (d.date != null && d.date.compareTo(date) == 0) {
                if (type == 1) d.morning = give;
                if (type == 2) d.noon = give;
                if (type == 3) d.evening = give;
                d.save();
                changed = true;
            }
        }

        if (!changed) {
            MedicationDate d = new MedicationDate();
            d.date = date;
            d.treatmentPlan = treatmentPlan;
            if (type == 1) d.morning = give;
            if (type == 2) d.noon = give;
            if (type == 3) d.evening = give;
            d.save();
            treatmentPlan.medicationDates.add(d);
        }

        renderText("success");
    }
}
