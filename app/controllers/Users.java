package controllers;

import models.*;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.List;

@With(Secure.class)
public class Users extends CRUD {
    public static User getUser() {
        User user = (User) renderArgs.get("user");
        if (user == null) redirect("/logout");
        return user;
    }

    public static int passLength() {
        if (session.get("passLength") != null) return Integer.parseInt(session.get("passLength"));
        else return 0;
    }

    public static void blank(String registerChar1, String registerChar2, String registerNum, String firstName, String phone) throws Exception {
        User owner = getUser();
        if (owner.isUser()) forbidden();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Model object = (Model) constructor.newInstance();
        try {
            List<UserSex> userSexes = UserSex.findAll();
            List<UserRole> userRoles;
            //регистр, нэр, утасны дугаарын аль нэг нь хоосон биш байвал үйлчлүүлэгчийн бүртгэл
            if (registerChar1 == null) userRoles = getUserRoles();
            else userRoles = UserRole.find("id=5 ORDER BY queue").fetch();
            List<Hospital> hospitals;
            if (owner.isRoleAdmin()) {
                hospitals = Hospital.findAll();
            } else {
                hospitals = Hospital.find("id = ?1", owner.hospital.id).fetch();
            }
            render(userSexes, userRoles, hospitals, registerChar1, registerChar2, registerNum, firstName, phone);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object);
        }
    }

    public static List<UserRole> getUserRoles() {
        User us = Users.getUser();
        List<UserRole> userRoles;
        if (us.isRoleAdmin() || us.isManager) userRoles = UserRole.find("id!=1 ORDER BY queue").fetch();
        else userRoles = UserRole.find("id=5 ORDER BY queue").fetch();
        return userRoles;
    }

    public static void create(String registerNum, String registerChar1, String registerChar2, String sectorUsers, String certificateNo,
                              String degree, String position, String direction) throws Exception {

        User owner = getUser();
        String[] sectorIds = null;
        if (sectorUsers != null) {
            sectorIds = sectorUsers.split(", ");
        }

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        User object = (User) constructor.newInstance();
        notFoundIfNull(object);
        Binder.bind(object, "object", params.all());

//        if (!object.hospital.id.equals(owner.hospital.id)) forbidden();
        //Хэрэглэгч үүсгэж байгаа хүн өөрөөсөө дээж түвшний эрхийг өгөхөд access denied
        if (object.userRole.queue.intValue() < owner.userRole.queue.intValue()) forbidden();

        if ((owner.isRoleAdmin() || owner.isManager) && registerNum == null) {
            object.username = object.username.toUpperCase();
        } else {
            if (registerNum != null && registerNum.length() > 0)
                object.username = String.valueOf(object.registerChar1) + String.valueOf(object.registerChar2) + object.registerNumber;
            else {
                object.username = object.firstName + object.phone1;
                object.registerChar1 = 'Ъ';
                object.registerChar2 = 'Ъ';
            }
            object.password = Functions.randomPassword();
            object.firstPassword = object.password;
        }

        object.owner = owner;
//        validation.valid(object);

        long count = User.find("username=?1 AND active=true", object.username).fetch().size();
        long register_count = 0L;
        if (registerNum != null && !registerNum.equals(""))
            register_count = User.find("registerChar1=?1 AND registerChar2=?2 AND registerNumber=?3 AND active=true", object.registerChar1, object.registerChar2, object.registerNumber).fetch().size();

        if (validation.hasErrors() || count > 0 || object.profilePicture.length() == 0 || register_count > 0) {
            if (object.profilePicture.length() == 0) renderArgs.put("error", "Зураг заавал оруулах ёстой!");
            else if (register_count > 0) {
                renderArgs.put("error", "Регистрийн дугаар давхардаж байна!");
            } else if (count > 0) {
                renderArgs.put("error", "Нэвтрэх нэр давхардаж байна!");
            } else renderArgs.put("error", Messages.get("crud.hasErrors"));
            List<UserRole> userRoles = getUserRoles();
            List<Hospital> hospitals;
            if (owner.isRoleAdmin()) {
                hospitals = Hospital.findAll();
            } else {
                hospitals = Hospital.find("id = ?1", owner.hospital.id).fetch();
            }
            List<UserSex> userSexes = UserSex.findAll();
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object, userRoles, hospitals, userSexes);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object, userRoles, registerNum, registerChar1, registerChar2);
            }
        }

        if (object.isRoleDoctor()) {
            Doctor doctor = new Doctor();
            doctor.certificateNo = certificateNo;
            doctor.degree = degree;
            doctor.position = position;
            doctor.direction = direction;
            doctor.user = object;
            object.doctor = doctor;
        }

        object.lastNameLength = lastNameWordLength(true, object);
        object.password = Functions.getSha1String(object.password);
        object._save();

        if (sectorIds != null) {
            for (int i = 0; i < sectorIds.length; i++) {
                Sector sector = Sector.findById(Long.parseLong(sectorIds[i].toString()));
                SectorUserRel sectorUser = new SectorUserRel();
                sectorUser.sector = sector;
                sectorUser.user = object;
                sectorUser._save();
            }
        }

        if (owner.isRoleAdmin()) {
            flash.success(Messages.get("crud.created", type.modelName));
            redirect(request.controller + ".list");
        }
        if (registerNum != null) {
            Inspection inspection = Inspections.addUserRegFunc(object.id, Functions.convertHourNull(new Date()));
            Inspections.register(inspection.id, 1L);
        } else {
            flash.success(Messages.get("crud.created", type.modelName));
            MainRoot.root();
        }

        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
    }

    public static void show(Long id) throws Exception {

        User owner = getUser();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        User object = User.findById(id);
        notFoundIfNull(object);
        User loginUser = getUser();
        try {
            List<Hospital> hospitals;
            if (owner.isRoleAdmin()) {
                hospitals = Hospital.findAll();
            } else {
                hospitals = Hospital.find("id = ?1", owner.hospital.id).fetch();
            }
            List<UserRole> userRoles = getUserRoles();
            List<UserSex> userSexes = UserSex.findAll();
            render(object, userRoles, userSexes, hospitals, loginUser);
        } catch (TemplateNotFoundException e) {
            render("CRUD/show.html", type, object, loginUser);
        }
    }

    public static void save(Long id, String sectorUsers, String certificateNo, String degree, String position, String direction) {
        ObjectType type = ObjectType.get(getControllerClass());

        String[] sectorIds = null;
        if (sectorUsers != null) {
            sectorIds = sectorUsers.split(", ");
        }
        notFoundIfNull(type);
        User owner = getUser();
        User object = User.findById(id);
//        int permission = owner.getPermission(object);
//        if (!(object.id.compareTo(owner.id) == 0 || permission > 0)) forbidden();
        notFoundIfNull(object);
        //хуучин мэдээллийг түр хадгалах
        String beforePassword = object.password;
        String proPic = object.profilePicture;
        String username = object.username;
        Binder.bindBean(params.getRootParamNode(), "object", object);
        //Update хийгдсэн мэдээлэл
        User updatedUser = new User();
        Binder.bindBean(params.getRootParamNode(), "object", updatedUser);

        validation.valid(object);
        object.password = beforePassword;
        object.username = object.username.toUpperCase();
        long count = User.find("id!=?1 AND username=?2 AND active=true", object.id, object.username).fetch().size();
        if (validation.hasErrors() || count > 0 || object.profilePicture.length() == 0) {
            if (object.profilePicture.length() == 0) renderArgs.put("error", "Зураг заавал оруулах ёстой!");
            else if (count > 0) renderArgs.put("error", "Нэвтрэх нэр давхардаж байна!");
            else renderArgs.put("error", Messages.get("crud.hasErrors"));
            List<UserRole> userRoles = getUserRoles();
            List<Hospital> hospitals = Hospital.find("ORDER BY name").fetch();
            List<UserSex> userSexes = UserSex.findAll();
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object, userRoles, hospitals, userSexes);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object, userRoles);
            }
        }

        if (!proPic.equals(object.profilePicture) && !object.profilePicture.contains("avatars")) {
            if (!proPic.contains("avatars")) Functions.deleteFileSingle(proPic);
            object.profilePicture = FileUploader.createProfilePicture(object.profilePicture, object.x, object.y, object.w, object.h);
        }
        object._save();

        Doctor doctor;
        doctor = object.doctor;

        if (object.isRoleDoctor()) {
            if (doctor == null) doctor = new Doctor();
            doctor.certificateNo = certificateNo;
            doctor.degree = degree;
            doctor.position = position;
            doctor.direction = direction;
            doctor.user = object;
            object.doctor = doctor;
        }

        object.isManager = updatedUser.isManager;
        object.lastNameLength = lastNameWordLength(true, object);
        object._save();

        List<SectorUserRel> rels = SectorUserRel.find("user_id=?1", object.id).fetch();
        for (SectorUserRel rel : rels) rel._delete();
        if (sectorIds != null) {
            for (int i = 0; i < sectorIds.length; i++) {
                Sector sector = Sector.findById(Long.parseLong(sectorIds[i].toString()));
                SectorUserRel sectorUser = new SectorUserRel();
                sectorUser.sector = sector;
                sectorUser.user = object;
                sectorUser._save();
            }
        }

        if (!username.equals(object.username) && owner.id.compareTo(object.id) == 0) {
            redirect("/logout");
        } else flash.success(Messages.get("crud.saved", object));
        if (params.get("_save") != null) {
            if (owner.isRoleAdmin()) redirect(request.controller + ".list");
            else redirect(request.controller + ".show", object._key());
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void savePassword(String oldPass, String newPass, String newRepeatPass) {
        User user = Users.getUser();
        if (!user.password.equals(Functions.getSha1String(oldPass))) {
            renderText("Хуучин нууц үг буруу байна!");
        } else if (newPass == null || newPass.length() == 0) {
            renderText("Шинэ нүүц үгээ бичнэ үү!");
        } else if (!newPass.equals(newRepeatPass)) {
            renderText("Дахин бичсэн нууц үг зөрж байна!");
        } else {
            user.password = Functions.getSha1String(newPass);
            user.save();
            session.put("passLength", newPass.length());
            renderText("success");
        }
    }

    public static void list() {
        User user = Users.getUser();
        if (user.isUser()) forbidden();
        List<Hospital> hospitals;
        if (user.isRoleAdmin()) {
            hospitals = Hospital.findAll();
        } else {
            hospitals = Hospital.find("id = ?1", user.hospital.id).fetch();
        }
        List<UserRole> userRoles = getUserRoles();
        render(hospitals, userRoles);
    }

    public static void listLoad(int CurrentPageNumber, String userName, Long userRole, Long hospital,
                                boolean isActive, boolean nonActive) {

        User owner = Users.getUser();

        String qr = "SELECT DISTINCT u FROM tb_user u", where = " WHERE";

        where += " u.userRole.id!=1 ";
        if (!owner.isRoleAdmin()) where += " and u.hospital.id=" + owner.hospital.id;
        if (!owner.isManager && !owner.isRoleAdmin()) where += " and u.userRole.id=5";

        if (isActive) where += " AND u.active=true";
        else if (nonActive) where += " AND u.active=false";

        if (userName.length() > 0) where += " AND u.firstName LIKE '%" + userName + "%'";
        if (hospital > 0) where += " AND u.hospital.id=" + hospital;
        if (userRole > 0) where += " AND u.userRole.id=" + userRole;

        qr += where + " ORDER BY u.firstName";
        int pageLimit = 10;
        int totalSize = User.find(qr).fetch().size();
        int MaxPageNumber = totalSize / pageLimit;
        if (totalSize % pageLimit != 0) MaxPageNumber++;
        List<User> objects = User.find(qr).fetch(CurrentPageNumber, pageLimit);
        render(objects, totalSize, MaxPageNumber, CurrentPageNumber);
    }

    public static void loadSector(Long hid) {
        List<Sector> sectors = Sector.find("hospital.id=?1 ", hid).fetch();
        render(sectors);
    }

    public static int lastNameWordLength(boolean isnew, User user) {
        if (user.lastName == null) return 0;
        List<User> users;
        if (isnew) users = User.find("active=true").fetch();
        else users = User.find("active=true and id!=?1", user.id).fetch();
        int count = 1;
        while (count <= user.lastName.length() && count <= 3) {
            if (lastNameMatch(users, user, count)) count++;
            else break;
        }
        if (count > user.lastName.length()) return user.lastName.length();
        return count;
    }

    public static boolean lastNameMatch(List<User> users, User own, int count) {
        for (User user : users) {
            if (user.firstName.toLowerCase().equals(own.firstName.toLowerCase())
                    && user.lastName.toLowerCase().startsWith(own.lastName.toLowerCase().substring(0, count))) {
                if (user.lastNameLength < count) {
                    user.lastNameLength = count;
                    user._save();
                }
                return true;
            }
        }
        return false;
    }
}
