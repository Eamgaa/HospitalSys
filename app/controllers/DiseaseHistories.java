package controllers;

import models.*;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 9/22/18.
 */
@With(Secure.class)
public class DiseaseHistories extends CRUD {

    public static void list() {
        Hospital hospital = Users.getUser().hospital;
        String query = "";
        query += "SELECT DISTINCT us FROM tb_user us INNER JOIN us.bedTreatments AS bt WHERE bt.hospital.id = " + Users.getUser().hospital.id;
        List<User> objects = User.find(query).fetch();
        render(objects);
    }

    public static void create(Long id, String[] singleAnswer, String[] multipleAnswer, String[] inputAnswer) {

        BedTreatment bedTreatment = BedTreatment.findById(id);
        DiseaseHistory object = bedTreatment.diseaseHistory;
        if (object == null) {
            object = new DiseaseHistory();
        }

//        System.out.println("Odnoo  --- " + id);
//
//        for (int i = 0; i < singleAnswer.length; i++) {
//            System.out.println("Odnoo  --- " + singleAnswer[i]);
//        }
//
//        for (int i = 0; i < multipleAnswer.length; i++) {
//            System.out.println("Odnoo  --- " + multipleAnswer[i]);
//        }
//
//        for (int i = 0; i < inputAnswer.length; i++) {
//            System.out.println("Odnoo  --- " + inputAnswer[i]);
//        }
    }

    public static void patient(Long id) {
        Hospital hospital = Users.getUser().hospital;
        User patient = User.findById(id);
        List<BedTreatment> bedTreatments = BedTreatment.find("hospital_id=?1 AND customer_id=?2 ORDER by startDate DESC", hospital.id, id).fetch();
        render(bedTreatments, patient);
    }

    public static void drugList(Long id) {
        BedTreatment object = BedTreatment.findById(id);
        List<Date> dates = Functions.dateToDate(object.startDate, object.endDate);
        List<TreatmentPlan> treatmentPlans = TreatmentPlan.find("type=?1 AND bedTreatment_id=?2", 2, id).fetch();
        render(object, treatmentPlans, dates);
    }

    public static void nurseExamination(Long id) {
        List<NurseExamination> objects = NurseExamination.find("bedTreatment_id=?1 ORDER BY date", id).fetch(1, 6);
        BedTreatment bedTreatment = BedTreatment.findById(id);
        render(objects, bedTreatment);
    }

    public static void dailyNursing(Long id) {
        BedTreatment bedTreatment = BedTreatment.findById(id);
        List<DailyNursing> objects = DailyNursing.find("bedTreatment_id=?1", id).fetch();
        render(objects, bedTreatment);
    }

    public static void nursingNote(Long id) {
        BedTreatment object = BedTreatment.findById(id);
        render(object);
    }

    public static void diseaseHistory(Long id) {
        Hospital hospital = Users.getUser().hospital;
        BedTreatment bedTreatment = BedTreatment.findById(id);
        List<DHQuestion> questions = DHQuestion.findAll();

        if (bedTreatment.diseaseHistory == null) {
            DiseaseHistory diseaseHistory = new DiseaseHistory();
            List<DHAnswer> dhAnswers = new ArrayList<DHAnswer>();
            for (DHQuestion question : questions) {
                DHAnswer dhAnswer = new DHAnswer();
                dhAnswer.dhQuestion = question;
                dhAnswers.add(dhAnswer);
            }
            diseaseHistory.dhAnswers = dhAnswers;
            bedTreatment.diseaseHistory = diseaseHistory;
        }
        render(bedTreatment, hospital, questions);
    }

    public static void diseaseHistoryPrint(Long id) {
        Hospital hospital = Users.getUser().hospital;
        BedTreatment bedTreatment = BedTreatment.findById(id);
        List<DHQuestion> questions = DHQuestion.findAll();

        if (bedTreatment.diseaseHistory == null) {
            DiseaseHistory diseaseHistory = new DiseaseHistory();
            List<DHAnswer> dhAnswers = new ArrayList<DHAnswer>();
            for (DHQuestion question : questions) {
                DHAnswer dhAnswer = new DHAnswer();
                dhAnswer.dhQuestion = question;
                dhAnswers.add(dhAnswer);
            }
            diseaseHistory.dhAnswers = dhAnswers;
            bedTreatment.diseaseHistory = diseaseHistory;
        }
        render(bedTreatment, hospital, questions);
    }

    public static void vitalIndices(Long id) {
        List<VitalIndex> firstPage = VitalIndex.find("bedTreatment_id=?1 ORDER BY date", id).fetch(1, 5);
        List<VitalIndex> secondPage = VitalIndex.find("bedTreatment_id=?1 ORDER BY date", id).fetch(2, 5);
        List<Integer> pulses = new ArrayList<Integer>();
        List<Integer> respirations = new ArrayList<Integer>();
        List<Float> temperatures = new ArrayList<Float>();
        for (int i : Consts.pulses) {
            pulses.add(i);
        }
        for (int i : Consts.respirations) {
            respirations.add(i);
        }
        for (float i : Consts.temperatures) {
            temperatures.add(i);
        }
        render(pulses, respirations, temperatures, firstPage, secondPage);
    }
}
