package controllers;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class Consts {

    public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public static final String Admin = "Admin";
    public static final String Doctor = "Doctor";
    public static final String Nurse = "Nurse";
    public static final String Reception = "Reception";
    public static final String User = "User";



    public static final int canAdmin = 1;
    public static final int canWorker = 2;

    public static final String uploadHospitallogoPath = "/public/images/hospitalLogos/";

    public static final int[] pulses= {150, 140, 130, 120, 110, 100, 90, 80, 70, 60, 50, 45};
    public static final int[] respirations= {55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 0};
    public static final float[] temperatures= {40.5f, 40f, 39.5f, 39f, 38.5f, 38, 37.5f, 37f, 36.5f, 36f, 35.5f, 35f};


    //Daatgaltai holbogdoh header
    public static final String requestHeaderName = "Authorization";
    public static final String requestHeaderValue = "Basic VV9mZl05Qmp5WlhMbUcmZHcmOlo3JHtFenlyRDRheUN9RkxkJg==";
}
