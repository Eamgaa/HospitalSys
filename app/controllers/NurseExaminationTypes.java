package controllers;

import models.NurseExamination;
import models.NurseExaminationType;
import play.mvc.With;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suren on 9/20/18.
 */
@With(Secure.class)
public class NurseExaminationTypes extends CRUD {
    public static void blank(Long id){
        NurseExaminationType object;
        List<NurseExaminationType> subTypes = new ArrayList<NurseExaminationType>();
        if (id != null) {
             object = NurseExaminationType.findById(id);
        } else object = new NurseExaminationType();
        NurseExaminationType parentType = object.parentType;
        if (parentType != null) subTypes= parentType.subTypes;
        List<NurseExaminationType> typeList = NurseExaminationType.find("ORDER BY id DESC").fetch();
        render(parentType, object, subTypes, typeList);
    }

    public static void create(Long id, Long parentType, String name, Integer value) {
        NurseExaminationType object;
        if (id != null) {
            object = NurseExaminationType.findById(id);
        } else object = new NurseExaminationType();
        if (parentType != 0) {
            object.parentType = NurseExaminationType.findById(parentType);
        }
        object.name = name;
        object.value = value;
        object._save();
        list();
    }

    public static void list() {
        List<NurseExaminationType> objects = NurseExaminationType.findAll();
        render(objects);
    }
}
