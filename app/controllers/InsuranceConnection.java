package controllers;

import com.google.gson.Gson;
import controllers.MyClass.HIReceipt;
import controllers.MyClass.HIToken;
import models.User;
import okhttp3.*;

import java.io.IOException;
import java.lang.String;
import java.util.Date;

public class InsuranceConnection extends CRUD {

    public static String getToken() throws IOException{
        User usr = Users.getUser();
        String url = "http://ws.emd.gov.mn/oauth/token?grant_type=password&username="+usr.hospital.insuranceUsername+"&password="+usr.hospital.insurancePassword;
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .build();
        Request request = new Request.Builder()
                .header(Consts.requestHeaderName,Consts.requestHeaderValue)
                .url(url)
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static void checkNumber(String number) throws IOException {
        String healthInsuranceToken = "";
        try {
            healthInsuranceToken = getToken();
            System.out.println("\n hitoken  -   "+healthInsuranceToken);
        } catch (IOException e) {
            e.printStackTrace();
            healthInsuranceToken = "failed";
            System.out.print("\n Token failed");
        }
        if (healthInsuranceToken.equals("failed")) {
            render(healthInsuranceToken);
        }
        Gson gson = new Gson();
        HIToken hiToken= gson.fromJson(healthInsuranceToken, HIToken.class);
        OkHttpClient client = new OkHttpClient();
        String url = "http://ws.emd.gov.mn/receipt/checkNumber?receiptNumber=" + number + "&access_token=" + hiToken.access_token;
        System.out.println("url = "+url);
        Request request = new Request.Builder()
                .header(Consts.requestHeaderName,Consts.requestHeaderValue)
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        String receiptData = response.body().string();
        System.out.println(receiptData);
        HIReceipt receipt = gson.fromJson(receiptData, HIReceipt.class);
        System.out.print("\nreceipt - "+receipt.receiptDate);
        Date date = new Date(Long.parseLong(receipt.receiptDate));
        Date expireDate = new Date(Long.parseLong(receipt.receiptExpireDate));
        Date printedDate = new Date(Long.parseLong(receipt.receiptPrintedDate));
        System.out.print(date + "--"+expireDate + "--"+printedDate);
        render(receipt, expireDate);
    }

    public static String getApprove(String number, String diagCode, String status) throws IOException {
        String healthInsuranceToken = "";
        try {
            healthInsuranceToken = getToken();
            System.out.println("\n hitoken  -   "+healthInsuranceToken);
        } catch (IOException e) {
            e.printStackTrace();
            healthInsuranceToken = "failed";
            System.out.print("\n Token failed");
        }
        if (healthInsuranceToken.equals("failed")) {
            return  healthInsuranceToken;
        }
        if (healthInsuranceToken.equals("failed")) {
            return healthInsuranceToken;
        }
        Gson gson = new Gson();
        HIToken hiToken= gson.fromJson(healthInsuranceToken, HIToken.class);
        OkHttpClient client = new OkHttpClient();
        String url = "http://ws.emd.gov.mn/receipt/approve?receiptNumber=" + number + "&access_token=" + hiToken.access_token +
                "&diagCode=" + diagCode + "&status=" + status;
        System.out.println("url = "+url);
        Request request = new Request.Builder()
                .header(Consts.requestHeaderName,Consts.requestHeaderValue)
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        String receiptData = response.body().string();
        System.out.println(receiptData);
        HIReceipt receipt = gson.fromJson(receiptData, HIReceipt.class);
        return "success";
    }
}
