package controllers;

import models.*;
import play.mvc.With;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by suren on 9/20/18.
 */
@With(Secure.class)
public class NurseExaminations extends CRUD {
    public static void blank(Long id) {
        BedTreatment bt = BedTreatment.findById(id);
        List<NurseExaminationType> parents = NurseExaminationType.find("parentType IS NULL").fetch();
        NurseExaminationAnswer answer = NurseExaminationAnswer.find("category=?1", 0).first();
        List<NurseExaminationAnswer> type1 = NurseExaminationAnswer.find("category=?1", 1).fetch();
        List<NurseExaminationAnswer> type2 = NurseExaminationAnswer.find("category=?1", 2).fetch();
        List<NurseExaminationAnswer> type3 = NurseExaminationAnswer.find("category=?1", 3).fetch();
        List<NurseExaminationAnswer> type4 = NurseExaminationAnswer.find("category=?1", 4).fetch();
        List<NurseExaminationAnswer> type5 = NurseExaminationAnswer.find("category=?1", 5).fetch();
        List<NurseExaminationAnswer> type6 = NurseExaminationAnswer.find("category=?1", 6).fetch();
        ArrayList<List<NurseExaminationAnswer>> answers =new ArrayList<List<NurseExaminationAnswer>>();
        answers.add(type1);
        answers.add(type2);
        answers.add(type3);
        answers.add(type4);
        answers.add(type5);
        answers.add(type6);
        List<NurseExamination> nurseExaminations = bt.nurseExaminations;
        render(parents, answers, answer, bt, nurseExaminations);
    }

    public static void show(Long id) {
        NurseExamination object = NurseExamination.findById(id);
        List<NurseExaminationType> parents = NurseExaminationType.find("parentType IS NULL").fetch();
        NurseExaminationAnswer answer = NurseExaminationAnswer.find("category=?1", 0).first();
        List<NurseExaminationAnswer> type1 = NurseExaminationAnswer.find("category=?1", 1).fetch();
        List<NurseExaminationAnswer> type2 = NurseExaminationAnswer.find("category=?1", 2).fetch();
        List<NurseExaminationAnswer> type3 = NurseExaminationAnswer.find("category=?1", 3).fetch();
        List<NurseExaminationAnswer> type4 = NurseExaminationAnswer.find("category=?1", 4).fetch();
        List<NurseExaminationAnswer> type5 = NurseExaminationAnswer.find("category=?1", 5).fetch();
        List<NurseExaminationAnswer> type6 = NurseExaminationAnswer.find("category=?1", 6).fetch();
        ArrayList<List<NurseExaminationAnswer>> answers =new ArrayList<List<NurseExaminationAnswer>>();
        answers.add(type1);
        answers.add(type2);
        answers.add(type3);
        answers.add(type4);
        answers.add(type5);
        answers.add(type6);
        render(object, parents, answers, answer);
    }

    public static void create (Long id, String date,  Long bt_id, Long[] questions, Long[] answers) {
        BedTreatment bt = BedTreatment.findById(bt_id);
        NurseExamination object;
        if (id != null && id != 0L) {
            object = NurseExamination.findById(id);
            List<NurseExaminationQA> qas = NurseExaminationQA.find("nurseExamination_id=?1", id).fetch();
            for (NurseExaminationQA qa: qas) qa.delete();
        } else {
            object = new NurseExamination();
            object.qaList = new ArrayList<NurseExaminationQA>();
        }
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try {
            object.date = dateTimeFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        object.bedTreatment = bt;
        for (int i=0; i<questions.length; i++) {
            NurseExaminationQA neqa = new NurseExaminationQA();
            neqa.examinationType = NurseExaminationType.findById(questions[i]);
            neqa.examinationAnswer = NurseExaminationAnswer.findById(answers[i]);
            neqa.nurseExamination = object;
            object.qaList.add(neqa);
        }
        object.save();
        bt.nurseExaminations.add(object);
        bt.save();
        renderText("success");
    }
}
