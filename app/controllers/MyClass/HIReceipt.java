package controllers.MyClass;

public class HIReceipt {
    public String id;
    public String receiptnumber;
    public String receiptDate;
    public String receiptType;
    public String receiptDiag;
    public String receiptExpireDate;
    public String receiptPrintedDate;
    public String status;
    public String hosOfficeName;
    public String hosName;
    public String hosSubOffName;
    public String cipherCode;
    public String tbltCount;
    public String patientLastName;
    public String patientFirstName;
    public String patientRegNo;
    public ReceiptDetails[] receiptDetails;
}
