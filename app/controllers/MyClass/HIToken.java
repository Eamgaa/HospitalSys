package controllers.MyClass;

public class HIToken {

    public String access_token;

    public String token_type;

    public String refresh_token;

    public String expires_in;

    public String scope;

    public String error;

    public String error_description;
}
