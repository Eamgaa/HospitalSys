package controllers.MyClass;

public class ReceiptDetails {
    public String id;
    public String tbltId;
    public String tbltName;
    public String tbltSize;
    public String tbltDesc;
    public String status;
    public String isDiscount;
    public String tbltType;
    public String tbltTypeName;
}
