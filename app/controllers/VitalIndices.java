package controllers;

import models.BedTreatment;
import models.User;
import models.VitalIndex;
import play.mvc.With;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 9/17/18.
 */
@With(Secure.class)
public class VitalIndices extends CRUD {

    public static void blank(Long id) {
        BedTreatment bedTreatment = BedTreatment.findById(id);
        List<User> nurses = new ArrayList<User>();
        for (User user: Users.getUser().hospital.users) {
            if (user.isRoleNurse()) {
                nurses.add(user);
            }
        }
        List<Date> dates = Functions.dateToDate(bedTreatment.startDate, bedTreatment.endDate);
        Date vital_date = new Date();
        vital_date = Functions.removeTime(vital_date);
        boolean change = true;
        for (Date d : dates){
            if (vital_date.compareTo(d) == 0) change = false;
        }
        if (change) vital_date = bedTreatment.startDate;
        VitalIndex object = VitalIndex.find("bedTreatment_id=?1 AND date=?2", id, vital_date).first();
        render(dates, nurses, bedTreatment, object, vital_date);
    }

    public static void blankInfo(Long bt_id, Date date) {
        VitalIndex object = VitalIndex.find("bedTreatment_id=?1 AND date=?2", bt_id, date).first();
        System.out.print("\n date - " + date);
        List<VitalIndex> objects = VitalIndex.find("bedTreatment_id=?1", bt_id).fetch();
        System.out.print("\n size - "+objects.size());
        if (object == null) object = new VitalIndex();
        List<User> nurses = new ArrayList<User>();
        for (User user: Users.getUser().hospital.users) {
            if (user.isRoleNurse()) {
                nurses.add(user);
            }
        }
        render(object, nurses);
    }

    public static void create(Date date, Long id, String dayPulses, String nightPulses, String dayRespirations,
                              String nightRespirations, String dayTemperatures, String nightTemperatures, Long dayPressure,
                              Long nightPressure, Integer mealCount, Long bodyWeight, Integer dayDung, Integer nightDung,
                              Integer dayPiss, Integer nightPiss, Long morningNurse, Long dayNurse, Long eveningNurse) throws Exception{
        VitalIndex vitalIndex = VitalIndex.find("bedTreatment_id=?1 AND date=?2", id, date).first();
        System.out.print("\ndate - "+date+"id - "+id + "dayPulses - "+dayPulses);
        if (vitalIndex == null) {
            vitalIndex = new VitalIndex();
            vitalIndex.bedTreatment = BedTreatment.findById(id);
            vitalIndex.date = date;
        }
        vitalIndex.dayPulses = dayPulses;
        vitalIndex.nightPulses = nightPulses;
        vitalIndex.dayRespirations = dayRespirations;
        vitalIndex.nightRespirations = nightRespirations;
        vitalIndex.dayTemperatures = dayTemperatures;
        vitalIndex.nightTemperatures = nightTemperatures;
        vitalIndex.dayPressure = dayPressure;
        vitalIndex.nightPressure = nightPressure;
        vitalIndex.mealCount = mealCount;
        vitalIndex.bodyWeight = bodyWeight;
        vitalIndex.dayDung = dayDung;
        vitalIndex.nightDung = nightDung;
        vitalIndex.dayPiss = dayPiss;
        vitalIndex.nightPiss = nightPiss;
        vitalIndex.morningNurse = User.findById(morningNurse);
        vitalIndex.dayNurse = User.findById(dayNurse);
        vitalIndex.eveningNurse = User.findById(eveningNurse);
        vitalIndex._save();
        renderText("success");
    }
}
