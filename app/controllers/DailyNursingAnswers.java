package controllers;

import models.DailyNursingAnswer;
import play.mvc.Controller;
import play.mvc.With;

import java.util.List;

@With(Secure.class)
public class DailyNursingAnswers extends CRUD {
    public static void blank(Long id) {
        DailyNursingAnswer object;
        if (id != null) {
            object = DailyNursingAnswer.findById(id);
        } else object = new DailyNursingAnswer();
        render(object);
    }

    public static void create(Long id, Integer category, String issue){
        DailyNursingAnswer object;
        if (id != 0L) {
            object = DailyNursingAnswer.findById(id);
        } else object = new DailyNursingAnswer();
        object.category = category;
        object.issue = issue;
        object._save();
        list();
    }

    public static void list() {
        List<DailyNursingAnswer> objects = DailyNursingAnswer.findAll();
        render(objects);
    }
}
