package controllers;

import models.Hospital;
import models.Sector;
import models.SectorUserRel;
import models.User;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by suren on 7/16/18.
 */

@With(Secure.class)
public class Sectors extends CRUD {
    public static void list(Long hid) {
        ObjectType type = ObjectType.get(getControllerClass());
        Hospital hospital = Users.getUser().hospital;
        System.out.print(hospital.id);
        notFoundIfNull(type);
        List<Sector> sectors = Sector.find("hospital_id=?1 ORDER BY name", hospital.id).fetch();
        render(sectors, hospital);
    }

    public static void blank(Long hid) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Sector object = (Sector) constructor.newInstance();
        Hospital hospital = Hospital.findById(hid);
        try {
            render(type, object, hid, hospital);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, hospital);
        }
    }

    public static void create(Long hid) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Sector object = (Sector) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        Hospital hospital = Hospital.findById(hid);
        object.hospital = hospital;

        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            blank(hid);
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(hid);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(hid);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void save(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Sector object = Sector.findById(Long.parseLong(id));
        Hospital hospital=object.hospital;
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.hospital=hospital;
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            list(hospital.id);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void doctors(Long id){
        Sector object = Sector.findById(id);
        List<User> doctors = new ArrayList<User>();
        for (User user: object.hospital.users) {
            if (user.isRoleDoctor()) {
                doctors.add(user);
            }
        }
        render(doctors,  object);
    }
    public static void addDoctors(Long id, String doctorsList){
        Sector object = Sector.findById(id);
        List<SectorUserRel> rels = SectorUserRel.find("sector_id=?1", id).fetch();
        for (SectorUserRel rel: rels) {
            rel._delete();
        }
        String[] arrayDoc = doctorsList.split(",");
        for (int i = 0; i < arrayDoc.length; i++) {
            if (arrayDoc[i] != null && !arrayDoc[i].equals("null")) {
                SectorUserRel sectorUserRel = new SectorUserRel();
                sectorUserRel.user = User.findById(Long.parseLong(arrayDoc[i]));
                sectorUserRel.sector = object;
                object.sectorUserRels.add(sectorUserRel);
            }
        }
        object._save();
        renderText("success");
    }

    public static void show(Long id) {
        Sector object = Sector.findById(id);
        Hospital hospital = Users.getUser().hospital;
        render( object, hospital);
    }
}
