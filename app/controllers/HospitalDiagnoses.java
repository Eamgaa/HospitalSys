package controllers;

import models.Diagnose;
import models.Hospital;
import models.HospitalDiagnose;
import models.User;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.util.List;


/**
 * Created by user on 1/31/2019.
 */
@With(Secure.class)
public class HospitalDiagnoses extends CRUD {

    public static void blank() {

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);

        User user = Users.getUser();
        Hospital hospital = user.hospital;
        List<Diagnose> objects = getDiagnose(hospital.hospitalDiagnoses);

        render(objects, hospital);
    }

    public static void list() {
        User user = Users.getUser();
        List<HospitalDiagnose> diagnoses = HospitalDiagnose.find("hospital.id = ?1", user.hospital.id).fetch();
        render(diagnoses);
    }

    public static void hospitalDiagnoseLoad(int CurrentPageNumber, String searchWord) {
        int pageLimit = 20;
        int totalSize = Diagnose.findAll().size();
        int MaxPageNumber = totalSize / pageLimit;
        if (totalSize % pageLimit != 0) MaxPageNumber++;

        User user = Users.getUser();
        Hospital hospital = user.hospital;

        List<Diagnose> objects = Diagnose.find(getWhere(hospital.hospitalDiagnoses, searchWord)).fetch(CurrentPageNumber, pageLimit);
        render(objects, totalSize, MaxPageNumber, CurrentPageNumber);
    }

    public static void hospitalDiagnoseSave(String selectedDiagnoses) {
        User user = Users.getUser();
        Hospital hospital = user.hospital;

        String[] strArray;
        if (selectedDiagnoses.length() > 1 && selectedDiagnoses != null) {
            strArray = selectedDiagnoses.split(",");
            for (int i = 0; i < strArray.length; i++) {
                Diagnose diagnose = Diagnose.findById(Long.parseLong(strArray[i]));
                HospitalDiagnose hospitalDiagnose = new HospitalDiagnose();
                hospitalDiagnose.diagnose = diagnose;
                hospitalDiagnose.hospital = hospital;
                hospitalDiagnose._save();
            }
        }
        list();
    }

    public static String getWhere(List<HospitalDiagnose> hospitalDiagnoses, String searchWord) {
        String qr = searchWord.length() > 0 ? "name LIKE '%" + searchWord + "%' AND " : "";
        int length = hospitalDiagnoses.size(), i = 0;
        for (HospitalDiagnose hdiag : hospitalDiagnoses) {
            qr += "id!=" + hdiag.diagnose.id;
            if (i != length - 1) qr += " AND ";
            i++;
        }
        return qr;
    }

    public static List<Diagnose> getDiagnose(List<HospitalDiagnose> hospitalDiagnoses) {
        String qr = "";
        int length = hospitalDiagnoses.size(), i = 0;
        for (HospitalDiagnose hdiag : hospitalDiagnoses) {
            qr += "id!=" + hdiag.diagnose.id;
            if (i != length - 1) qr += " AND ";
            i++;
        }

        return Diagnose.find(qr + " ORDER BY name").fetch();
    }
}
