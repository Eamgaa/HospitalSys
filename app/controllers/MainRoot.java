package controllers;

import models.User;

/**
 * Created by mungu on 2018-07-25.
 */
public class MainRoot  extends CRUD {
    public static void root() {
        User user = Users.getUser();
        if (user.isRoleAdmin()) {
            Users.list();
        } else {
            Inspections.register(0L,1L);
        }
    }
}