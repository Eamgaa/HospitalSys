package controllers;

import models.*;
import play.data.binding.Binder;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 7/25/18.
 */
@With(Secure.class)
public class Hospitals extends CRUD {

    public static void blank(Long hid) {
        List<Aimag> aimags = Aimag.findAll();
        Hospital object = null;
        List<Sum> sumNames = null;
        List<Bag> bags = null;
        if (hid != null) {
            object = Hospital.findById(hid);
            sumNames = Sum.find("aimag.id=?1", object.aimag.id).fetch();
            bags = Bag.find("sumName.id=?1", object.sumName.id).fetch();
        }
        render(aimags, object, sumNames, bags);
    }

    public static void create(Long id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Hospital object;
        if (id!=null) {
            object = Hospital.findById(id);
        } else {
             object = (Hospital) constructor.newInstance();
        }
        Binder.bindBean(params.getRootParamNode(), "object", object);
        System.out.print("logo = "+ object.logo +"\n"+
                         "name = "+ object.name +"\n"+
                "direction = "+ object.direction +"\n"+
                "phone = "+ object.phone +"\n");
        validation.valid(object);
        if (validation.hasErrors()) {
            List<Aimag> aimags = Aimag.findAll();
            List<Sum> sumNames = null;
            List<Bag> bags = null;
            if (object.aimag != null) {
                sumNames = Sum.find("aimag.id=?1", object.aimag.id).fetch();
                if (object.sumName != null)
                bags = Bag.find("sumName.id=?1", object.sumName.id).fetch();
            }
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object, aimags, sumNames, bags);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object, aimags);
            }
        }
        if (id==null) {
            object.regDate = new Date();
            object.create();
        }
        object.active = true;
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            show(object.id);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank();
        }
        redirect(request.controller + ".list", object._key());
    }

    public static void list() {
        User owner = Users.getUser();
        if (!owner.isRoleAdmin() && !owner.isManager) forbidden();
        List<Hospital> hospitals = Hospital.findAll();
        render(hospitals);
    }

    public static void listLoad(int CurrentPageNumber, String name, boolean isActive, boolean nonActive) {
        String qr = "SELECT DISTINCT h FROM tb_hospital h", where = " WHERE";


        if (isActive) where += " h.active=true";
        else if (nonActive) where += " h.active=false";

        if (name.length() > 0) where += " AND h.name LIKE '%" + name + "%'";

        qr += where + " ORDER BY h.regDate";
        int pageLimit = 10;
        int totalSize = Hospital.find(qr).fetch().size();
        int MaxPageNumber = totalSize / pageLimit;
        if (totalSize % pageLimit != 0) MaxPageNumber++;
        List<Hospital> objects = Hospital.find(qr).fetch(CurrentPageNumber, pageLimit);
        render(objects, totalSize, MaxPageNumber, CurrentPageNumber);
    }

    public static void show(Long id){
        Hospital hospital = Hospital.findById(id);
        if (hospital == null) notFound();
        String address = hospital.getAddress();
        List<Module> modules = Module.findAll();
        render(hospital, address, modules);
    }

    public static void loadSumName(Long id) {
        List<Sum> sums = Sum.find("aimag.id=?1", id).fetch();
        render(sums);
    }

    public static void loadBagName(Long id) {
        List<Bag> bags = Bag.find("sumName.id=?1", id).fetch();
        render(bags);
    }

    public static void delete(Long id) {
        User user = Users.getUser();
        if (!user.isRoleAdmin()) forbidden();
        Hospital hospital = Hospital.findById(id);
        hospital.delete();
        list();
    }

    public static void changeModules(Long id, String hospitalModules, Integer port) {
        Hospital hospital = Hospital.findById(id);
        List<HospitalModule> hospitalModules1 = HospitalModule.find("hospital_id=?1", id).fetch();
        for (HospitalModule rel: hospitalModules1) rel._delete();
        String[] arrayModule = hospitalModules.split(",");
        for (int i = 0; i < arrayModule.length; i++) {
            if (arrayModule[i] != null && !arrayModule[i].equals("null")) {
                HospitalModule hospitalModule = new HospitalModule();
                hospitalModule.module = Module.findById(Long.parseLong(arrayModule[i]));
                hospitalModule.hospital = hospital;
                hospital.hospitalModules.add(hospitalModule);
            }
        }
        hospital.portNumber = port;
        hospital._save();
        renderText("success");
    }
}
