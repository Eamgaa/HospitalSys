package controllers;

import models.Drug;
import models.HospitalDrug;
import models.Hospital;
import models.User;
import play.data.binding.Binder;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by mungu on 2018-07-31.
 */
@With(Secure.class)
public class HospitalDrugs extends CRUD {
    public static void list() {
        User us = Users.getUser();
        Hospital hospital = us.hospital;
        notFoundIfNull(hospital);
        List<HospitalDrug> objects = HospitalDrug.find("hospital_id=?1", hospital.id).fetch();
        try {
            render(objects, hospital);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", objects, hospital);
        }
    }

    public static void blank() throws Exception {
        User us = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        HospitalDrug object = (HospitalDrug) constructor.newInstance();
        Hospital hospital = us.hospital;
        List<Drug> drugs = Drug.findAll();

        try {
            render(type, object, hospital, drugs);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, hospital, drugs);
        }
    }

    public static void show(Long id) {
        List<Drug> drugs = Drug.find("valid=?1", true).fetch();
        HospitalDrug object = HospitalDrug.findById(id);
        render(object, drugs);
    }

    public static void create() throws Exception {
        User us = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        HospitalDrug object = (HospitalDrug) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        Hospital hospital = us.hospital;
        object.hospital = hospital;

        validation.valid(object);

        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            blank();
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list();
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank();
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void save(String id) throws Exception {
        User us = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        HospitalDrug object = HospitalDrug.findById(Long.parseLong(id));
        Hospital hospital = us.hospital;
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.hospital = hospital;

        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            list();
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void delete(Long id) throws Exception {
        notFoundIfNull(id);
        HospitalDrug object = HospitalDrug.findById(id);
        try {
            object._delete();
        } catch (Exception e) {
            flash.error(play.i18n.Messages.get("crud.delete.error", object));
            redirect(request.controller + ".show", object._key());
        }
        flash.success(play.i18n.Messages.get("crud.deleted", object));
        list();
    }
}
