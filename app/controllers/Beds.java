package controllers;

import models.Bed;
import models.Room;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by suren on 7/18/18.
 */
@With(Secure.class)
public class Beds extends CRUD {
    public static void list(Long rid) {
        ObjectType type = ObjectType.get(getControllerClass());
        Room room = Room.findById(rid);
        notFoundIfNull(type);

        String where = "room.id=" + rid;
        List<Bed> objects = Bed.find("room_id=?1 ORDER BY bedNum", rid).fetch();
        try {
            render(type, objects, room);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, room);
        }
    }

    public static void blank(Long rid) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Bed object = (Bed) constructor.newInstance();
        Room room = Room.findById(rid);
        try {
            render(type, object, rid, room);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, room);
        }
    }

    public static void create(Long rid) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Bed object = (Bed) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        Room room = Room.findById(rid);
        object.room = room;

        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            blank(rid);
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(rid);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(rid);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void save(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Bed object = Bed.findById(Long.parseLong(id));
        Room room= object.room;
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.room=room;
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            list(room.id);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void delete(Long id, Long rid) throws Exception {
        Bed object = Bed.findById(id);
        try {
            object._delete();
        } catch (Exception e) {
            flash.error(play.i18n.Messages.get("crud.delete.error", object));
            redirect(request.controller + ".show", object._key());
        }
        flash.success(play.i18n.Messages.get("crud.deleted", object));
        list(rid);
    }
}
