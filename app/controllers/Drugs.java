package controllers;

import models.*;
import play.mvc.With;

import java.util.Date;
import java.util.List;

/**
 * Created by suren on 8/21/18.
 */
@With(Secure.class)
public class Drugs extends CRUD {

    public static void drugStore() {
        List<Drug> drugs = Drug.findAll();
        render(drugs);
    }

    public static void blank() {
        List<DrugDose> doses = DrugDose.findAll();
        List<DrugForm> drugForms = DrugForm.findAll();
        List<DrugManufacturer> drugManufacturers = DrugManufacturer.findAll();
        render( doses, drugForms, drugManufacturers);
    }

    public static void create(Long id, String registrationNumber, String internationalName, String sellName, Long manufacturer_id,
                              Long dose_id, Long drugForm_id, Boolean withRecipe, String object_image, Date validDate, String instruction) throws Exception{
        Drug object;
        if (id==null) {
            object = new Drug();
        } else object = Drug.findById(id);
        object.registrationNumber = registrationNumber;
        object.internationalName = internationalName;
        object.sellName = sellName;
        object.manufacturer = DrugManufacturer.findById(manufacturer_id);
        object.dose = DrugDose.findById(dose_id);
        object.drugForm = DrugForm.findById(drugForm_id);
        object.withRecipe = withRecipe;
        object.image = object_image;
        object.validDate = validDate;
        Date today = new Date();
        if (today.after(validDate)) object.valid = false;
        else object.valid = true;
        object.instruction = instruction;
        object._save();
        drugStore();
    }

    public static void list() {
        List<Drug> drugs = Drug.findAll();
        render(drugs);
    }

    public static void listLoad(int CurrentPageNumber, String drugName, Long userRole, Long hospital,
                                boolean isActive, boolean nonActive) {
        User owner = Users.getUser();

//        String qr = "SELECT DISTINCT u FROM tb_user u", where = " WHERE";
//
//        where += " u.userRole.id!=1 ";
//        if (!owner.isRoleAdmin()) where += " and u.hospital.id=" + owner.hospital.id;
//        if (!owner.isManager && !owner.isRoleAdmin()) where += " and u.userRole.id=5";
//
//        if (isActive) where += " AND u.active=true";
//        else if (nonActive) where += " AND u.active=false";
//
//        if (drugName.length() > 0) where += " AND u.firstName LIKE '%" + drugName + "%'";
//        if (hospital > 0) where += " AND u.hospital.id=" + hospital;
//        if (userRole > 0) where += " AND u.userRole.id=" + userRole;
//
//        qr += where + " ORDER BY u.firstName";
//        int pageLimit = 10;
//        int totalSize = User.find(qr).fetch().size();
//        int MaxPageNumber = totalSize / pageLimit;
//        if (totalSize % pageLimit != 0) MaxPageNumber++;
//        List<User> objects = User.find(qr).fetch(CurrentPageNumber, pageLimit);
        List<Drug> drugs = Drug.findAll();
        render(drugs, CurrentPageNumber);
    }
}
