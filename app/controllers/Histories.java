package controllers;

import com.google.gson.Gson;
import controllers.mn.mta.pos.classes.*;
import controllers.mn.mta.pos.handler.ClientHandler;
import models.*;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.joda.time.DateTime;
import play.mvc.With;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mungu on 2018-08-02.
 */
@With(Secure.class)
public class Histories extends CRUD {

    public static void loadPayment(Long ins_id) {
        User us = Users.getUser();
        Inspection inspection = Inspection.findById(ins_id);
        User customer = inspection.customer;
        List<History> histories = History.find("user.id = ?1 AND hospital.id = ?2 AND payment>0", inspection.customer.id, us.hospital.id).fetch();
        Long all = 0L;
        for (History his : histories) all += his.payment;
        render(ins_id, inspection, customer, histories, all);
    }

    public static void payment(Long history_id) {
        History history = History.findById(history_id);
        Date serviceDate = new Date();
        Inspection inspection = null;

        if (history.typeId == 1) {
            inspection = history.getInspection();
            serviceDate = inspection.serviceDate;
        }
        render(history, serviceDate, inspection);
    }

    public static void allPayment(Long customer_id) {
        User us = Users.getUser();
        User customer = User.findById(customer_id);
        List<History> histories = History.find("user.id = ?1 AND hospital.id = ?2 AND payment>0", customer.id, us.hospital.id).fetch();
        Date serviceDate = new Date();
        Long all = 0L;
        for (History his : histories) all += his.payment;
        render(histories, serviceDate, all, customer);
    }

    public static void savePayment(Long hid, String total, Long discount, String nowPay, String cardPay, String borrow, String customerNo) {
        Long totalPrice = 0L;
        System.out.println("Saving payment");
        if (total != null && total.length() > 0) {
            totalPrice = Long.parseLong(total.replaceAll("'", ""));
        }
        Long nowPayVal = 0L;
        if (nowPay != null && nowPay.length() > 0) {
            nowPayVal = Long.parseLong(nowPay.replaceAll("'", ""));
        }
        Long cardPayVal = 0L;
        if (cardPay != null && cardPay.length() > 0) {
            cardPayVal = Long.parseLong(cardPay.replaceAll("'", ""));
        }
        Long borrowVal = 0L;
        if (borrow != null && borrow.length() > 0) {
            borrowVal = Long.parseLong(borrow.replaceAll("'", ""));
        }
        History history = History.findById(hid);

        if ((totalPrice - (totalPrice * discount) / 100) != nowPayVal + cardPayVal + borrowVal) renderText(0);
        history.payment = borrowVal;

        //эхний төлөлт дээр л хөнгөлөлт хадгалах боломжтой
        if (history.historyLogRels.size() == 0) history.discount = discount;
        history._save();

        List<HistoryItem> historyItems = history.historyItems;
        for (HistoryItem item : historyItems) {
            item.paid = true;
            item._save();
        }

        HistoryLog log = new HistoryLog();
        log.createDate = new Date();
        log.payment = totalPrice;
        log.discount = discount;
        log.discountVal = Functions.getFloattoInt((float) (log.discount * totalPrice) / 100);
        log.cashPay = nowPayVal;
        log.cardPay = cardPayVal;
        log.borrow = borrowVal;
        log.customerNo = customerNo;
        log.owner = Users.getUser();

//        ХЭВТЭН ЭМЧЛҮҮЛЭХ
        if (history.typeId == 3) {
            BedTreatment bt = history.getBedTreatment();
            if (bt.receiptNumber != null) log.billSuffixId = bt.receiptNumber;
        }
        log.billIdSuffix = "";
        log._save();

        HistoryLogRel logRel = new HistoryLogRel();
        logRel.history = history;
        logRel.historyLog = log;
        logRel._save();

        log.historyLogRels = new ArrayList<HistoryLogRel>();
        log.historyLogRels.add(logRel);
        log._save();
        renderText(log.id);
    }

    public static void saveAllPayment(Long customer_id, String total, Long discount, String nowPay, String cardPay) {
        User us = Users.getUser();
        Long totalPrice = 0L;
        if (total != null && total.length() > 0) {
            totalPrice = Long.parseLong(total.replaceAll("'", ""));
        }
        Long cashPayVal = 0L;
        if (nowPay != null && nowPay.length() > 0) {
            cashPayVal = Long.parseLong(nowPay.replaceAll("'", ""));
        }
        Long cardPayVal = 0L;
        if (cardPay != null && cardPay.length() > 0) {
            cardPayVal = Long.parseLong(cardPay.replaceAll("'", ""));
        }

        List<History> histories = History.find("user.id = ?1 AND hospital.id = ?2 AND payment>0", customer_id, us.hospital.id).fetch();
        Long all = 0L;
        for (History his : histories) all += his.payment;
        if (!all.equals(totalPrice)) forbidden();
        HistoryLog log = new HistoryLog();
        log.createDate = new Date();
        log.payment = totalPrice;
        log.discount = discount;
        log.discountVal = Functions.getFloattoInt((float) (log.discount * totalPrice) / 100);
        log.cashPay = cashPayVal;
        log.cardPay = cardPayVal;
        log.owner = us;
        log._save();

        log.historyLogRels = new ArrayList<HistoryLogRel>();

        for (History his : histories) {
            HistoryLogRel rel = new HistoryLogRel();
            his.historyLogRels = new ArrayList<HistoryLogRel>();
            rel.historyLog = log;
            rel.history = his;
            rel._save();
            his.historyLogRels.add(rel);
            his.payment = 0L;
            his._save();
            log.historyLogRels.add(rel);
        }
        log._save();
        renderText(log.id);
    }

    public static void paymentPrint(Long lid) {
        HistoryLog log = HistoryLog.findById(lid);
        Date date = new Date();
        render(log, date);
    }

    public static void historyLog(Long uid) {
        User owner = Users.getUser();
        User customer = User.findById(uid);
        List<HistoryLog> historyLogs = HistoryLog.find("SELECT DISTINCT h FROM tb_history_log h LEFT JOIN h.historyLogRels AS rel WHERE rel.history.user.id='" +
                uid + "' AND rel.history.hospital.id ='" + owner.hospital.id + "' ORDER BY h.createDate").fetch();
        render(customer, historyLogs);
    }

    public static String generateBillIdSuffix() {
        Hospital hospital = Users.getUser().hospital;
        DateTime date = new DateTime();
        date = date.minusDays(1);
        if (hospital.suffixResetDay == null || hospital.suffixResetDay.before(date.toDate())) {
            hospital.suffixResetDay = new Date();
            hospital.lastBillIdSuffix = 100000;
        } else hospital.lastBillIdSuffix = hospital.lastBillIdSuffix + 1;
        hospital._save();
        Integer billIdSuffix = hospital.lastBillIdSuffix;
        return billIdSuffix.toString();
    }
}
