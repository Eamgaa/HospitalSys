package controllers;

import com.google.gson.Gson;
import controllers.mn.mta.pos.classes.*;
import controllers.mn.mta.pos.handler.ClientHandler;
import models.*;
import okhttp3.*;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.codec.textline.TextLineDecoder;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class EBarimt extends CRUD {

    public static String sendData(Long id) {
        Hospital hospital;
        if (id != null) {
            hospital = Hospital.findById(id);
        } else hospital = Users.getUser().hospital;
        int port = hospital.portNumber;
        IoConnector connector = new NioSocketConnector();
        connector.getFilterChain().addLast("logger", new LoggingFilter());
        connector.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory()));

        connector.setHandler(new ClientHandler("sendData"));
        ConnectFuture future = connector.connect(new InetSocketAddress(
                "localhost", port));
        future.awaitUninterruptibly();

        if (!future.isConnected()) {
            return "Bridge is not connected";
        }
        IoSession session = future.getSession();
        session.getConfig().setUseReadOperation(true);
        session.getCloseFuture().awaitUninterruptibly();
        String retValue = session.read().getMessage().toString();
        connector.dispose();
        return retValue;
    }

    public static String getInformatioin(Long id) {
        Hospital hospital;
        if (id != null) {
            hospital = Hospital.findById(id);
        } else hospital = Users.getUser().hospital;
        int port = hospital.portNumber;
        IoConnector connector = new NioSocketConnector();
        connector.getFilterChain().addLast("logger", new LoggingFilter());
        connector.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory()));

        connector.setHandler(new ClientHandler("getInformation"));
        ConnectFuture future = connector.connect(new InetSocketAddress(
                "localhost", port));
        future.awaitUninterruptibly();

        if (!future.isConnected()) {
            return "Bridge is not connected";
        }
        IoSession session = future.getSession();
        session.getConfig().setUseReadOperation(true);
        session.getCloseFuture().awaitUninterruptibly();
        String retValue = session.read().getMessage().toString();
        connector.dispose();
        return retValue;
    }

    public static String callFunction(String function, String data) {
        User usr = Users.getUser();
        int port = usr.hospital.portNumber;
        IoConnector connector = new NioSocketConnector();
        connector.getFilterChain().addLast("logger", new LoggingFilter());
        connector.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory()));

        connector.setHandler(new ClientHandler("callFunction|" + function+"|"+data));
        ConnectFuture future = connector.connect(new InetSocketAddress(
                "localhost", port));
        future.awaitUninterruptibly();

        if (!future.isConnected()) {
            return "Bridge is not connected";
        }
        IoSession session = future.getSession();
        session.getConfig().setUseReadOperation(true);
        session.getCloseFuture().awaitUninterruptibly();
        String retValue = session.read().getMessage().toString();
        connector.dispose();
        return retValue;
    }

    public static String checkAPI() {
        User usr = Users.getUser();
        int port = usr.hospital.portNumber;
        IoConnector connector = new NioSocketConnector();
        connector.getFilterChain().addLast("logger", new LoggingFilter());
        connector.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory()));

        connector.setHandler(new ClientHandler("checkAPI"));
        ConnectFuture future = connector.connect(new InetSocketAddress(
                "localhost", port));
        future.awaitUninterruptibly();

        if (!future.isConnected()) {
            return "Bridge is not connected";
        }
        IoSession session = future.getSession();
        session.getConfig().setUseReadOperation(true);
        session.getCloseFuture().awaitUninterruptibly();
//         System.out.println("dadfa " + session.read().getMessage().getClass().toString());
        String retValue = session.read().getMessage().toString();
        connector.dispose();
        return retValue;
    }

    public static void put(Long id) {

        User thisUser = Users.getUser();
        int port = thisUser.hospital.portNumber;
        HistoryLog log = HistoryLog.findById(id);

        Gson gson = new Gson();

        DecimalFormat df = new DecimalFormat("0.00");
        BillBankTransaction[] listBankTransAction = null;

        BillData data = new BillData();
        data.amount = df.format(log.payment);
        data.vat = "0.00";
        data.cashAmount = df.format(log.cashPay);
        data.nonCashAmount = df.format(log.cardPay);
        data.cityTax = "0.00";
        data.districtCode = thisUser.hospital.districtCode;
        data.posNo= "";
        data.customerNo = log.customerNo.length()==7?log.customerNo:"";
        data.billIdSuffix = log.billIdSuffix;
        data.billType = log.customerNo.length()==7?"3":"1";
        data.bankTransactions = listBankTransAction;
        data.billSuffixId = log.billSuffixId;

        System.out.println("cusNo - [" + data.customerNo + "] | billType - [" + data.billType+"]");
        BillDetail[] billStockArr = new BillDetail[100];
        List<BillDetail> billStockList = new ArrayList<BillDetail>();

        for (HistoryLogRel rel: log.historyLogRels) {
            for (HistoryItem item: rel.history.historyItems) {
                BillDetail stock = new BillDetail();
                stock.code = item.code;
                stock.name = item.name;
                stock.measureUnit = item.measureUnit;
                stock.qty = item.qty;
                stock.unitPrice = item.unitPrice;
                stock.totalAmount = item.totalAmount;
                stock.vat = "0.00";
                stock.barCode = "";
                stock.cityTax = "0.00";
                System.out.println("measureUnit - "+ stock.measureUnit);
                billStockList.add(stock);
            }
        }

        billStockArr = billStockList.toArray(new BillDetail[billStockList.size()]);
        data.stocks = billStockArr;
        String gsonStr = gson.toJson(data);

        IoConnector connector = new NioSocketConnector();

        connector.getFilterChain().addLast("logger", new LoggingFilter());
        connector.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory()));

        connector.setHandler(new ClientHandler("put|"
                + gsonStr));
        ConnectFuture future = connector.connect(new InetSocketAddress(
                "localhost", port));
        future.awaitUninterruptibly();

        if (!future.isConnected()) {
            renderText( "Bridge is not connected");
        }
        IoSession session = future.getSession();
        session.getConfig().setUseReadOperation(true);
        session.getCloseFuture().awaitUninterruptibly();
        String retValue = session.read().getMessage().toString();
        retValue += session.read().getMessage().toString();
        connector.dispose();
        System.out.println(retValue);
        BillData returnData = gson.fromJson(retValue, BillData.class);
        if (returnData.success.equals("true")) {
            System.out.print("\n-----------------\n"+returnData+"\n-----------------\n");
            System.out.println("\nqrdataLength - " + returnData.qrData.length());
            System.out.println("\nqrdata - " + returnData.qrData);
            log.billId = returnData.billId;
            try {
                log.date = Consts.dateTimeFormat.parse(returnData.date);
            } catch (Exception e) {
                System.out.println("exception while parsing log.date [" + returnData.date+"]");
                e.printStackTrace();
            }
            log._save();
            String eName ="";
            try {
                eName = checkCustomer(returnData.registerNo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            render(returnData, log, eName);
        } else {
            BillError billError = gson.fromJson(retValue, BillError.class);
            System.out.println("\nerrorCode - " + billError.errorCode);
            System.out.println("\nerrorMessage - " + billError.message);
            System.out.println("\nsuccess - " + billError.success);
            render(billError);
        }
    }

    public static void checkCustomerNo(String customerNo) throws IOException{
        String url = "http://info.ebarimt.mn/rest/merchant/info?regno="+customerNo;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        String responseString = response.body().string();
        Gson gson = new Gson();
        Customer customer = gson.fromJson(responseString, Customer.class);
        render(customer);
    }

    public static String checkCustomer(String customerNo) throws IOException{
        String url = "http://info.ebarimt.mn/rest/merchant/info?regno="+customerNo;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        String responseString = response.body().string();
        Gson gson = new Gson();
        Customer customer = gson.fromJson(responseString, Customer.class);
        return customer.name;
    }
}
