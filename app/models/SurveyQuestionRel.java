package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by user on 4/23/2019.
 */
@Entity(name = "tb_survey_question_rel")
public class SurveyQuestionRel extends Model {

    public String name;                                       //Асуулт

    public String qtype;                                      //хариултын төрөл: single, multiple, input

    public String answer;                                    //хариултууд

    public String answerCase;                                //нэмэлт хариулт гарч ирэх нөхцөл

    public String answerType;                                //нэмэлт хариултын төрөл: single, multiple, input

    public String extraAnswer;                               //нэмэлт хариултууд

    @ManyToOne
    public SurveyQuestionType questionType;

//    @ManyToOne
//    public Survey survey;
      public String toString() {
        return this.name;
    }

    public String[] getAnswers() {
        return answer.split("\\|");
    }

    public String[] getExtraAnswers() {
        return extraAnswer.split("\\|");
    }

}
