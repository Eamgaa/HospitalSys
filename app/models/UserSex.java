package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;

@Entity(name = "tb_user_sex")
public class UserSex extends Model {

    @Required
    public String name;

    public String toString() {
        return this.name;
    }

}