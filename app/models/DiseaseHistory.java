package models;

import controllers.CRUD;
import play.data.validation.Required;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "tb_disease_history")
public class DiseaseHistory extends BaseModel {

    public String closePersonName;

    public String closePersonPhone;

    //Төлбөрийн хэлбэр

    @Required
    public Date inspectionStartDate;

//    public String dsfsdfsds

    @Required
    @OneToOne
    public User owner;

    @Required
    @OneToOne
    public BedTreatment bedTreatment;

    @CRUD.Hidden
    @OneToMany(mappedBy = "diseaseHistory", cascade = CascadeType.ALL)
    public List<DHAnswer> dhAnswers;

}
