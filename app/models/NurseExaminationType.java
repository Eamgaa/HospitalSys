package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by suren on 9/20/18.
 */
@Entity(name = "tb_nurse_examination_type")
public class NurseExaminationType extends Model {

    public Integer value;

    @Required
    public String name;

    public Boolean normal;

    @OneToMany(mappedBy = "parentType", cascade = CascadeType.ALL)
    public List<NurseExaminationType> subTypes;

    @ManyToOne
    public NurseExaminationType parentType;

    public String toString() {
        return name;
    }

    public Integer childCount(){
        Integer childCount = 0;
        if (subTypes.size() == 0) return 1;
        for (NurseExaminationType rel: subTypes){
            childCount += rel.childCount();
        }
        return childCount;
    }
}
