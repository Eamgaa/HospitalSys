package models;

import controllers.CRUD;
import controllers.Consts;
import controllers.Functions;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Entity(name = "tb_user")
public class User extends Model {

    public Boolean active = true;

    @Required
    public String lastName;
    public int lastNameLength;

    @Required
    public String firstName;

    @Required
    public String username;

    @Required
    public String password;

    public String insuranceNumber;

    @Required
    public String profilePicture;
    public int x;
    public int y;
    public int w;
    public int h;

    @Required
    public Character registerChar1;
    @Required
    public Character registerChar2;
    @Required
    public String registerNumber;

    @CRUD.Hidden
    public String firstPassword;
    public Date lastAttempt;
    public int attempt;

    @Required
    @ManyToOne
    public UserSex userSex;

    @Required
    public String phone1;

    public String phone2;

    public String email;

    @Lob
    public String address;

    public Date birthDate;

    @Required
    @ManyToOne
    public UserRole userRole;

    @ManyToOne
    public Hospital hospital;

    public boolean isManager;

    @ManyToOne
    public User owner;

    @CRUD.Hidden
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    public List<SectorUserRel> sectorUserRels;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    public Doctor doctor;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    public List<History> histories;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    public List<Inspection> inspections;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    public List<BedTreatment> bedTreatments;

    @OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL)
    public List<BedTreatment> bedPatient;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    public List<NurseNote> nurseNotes;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    public List<SurveyInsUserRel> surveyInsUserRels;

    public String toString() {
        String la = getLastnameFirstCharacter();
        return la.length() > 0 ? la + "." + this.firstName : this.firstName;
    }

    private String getLastnameFirstCharacter() {
        if (this.lastName != null && this.lastName.length() > 0) return this.lastName.substring(0, 1);
        return "";
    }

    public String getRegister() {
        return String.valueOf(this.registerChar1) + String.valueOf(this.registerChar2) + this.registerNumber;
    }

    private boolean checkRole(String role) {
        return this.userRole.role.equals(role);
    }

    public boolean isRoleAdmin() {
        return this.checkRole(Consts.Admin);
    }

    public boolean isRoleDoctor() {
        return this.checkRole(Consts.Doctor);
    }

    public boolean isRoleNurse() {
        return this.checkRole(Consts.Nurse);
    }

    public boolean isRoleReception() {
        return this.checkRole(Consts.Reception);
    }

    public boolean isUser() {
        return this.checkRole(Consts.User);
    }

    public Long getBorrow(Long hospital_id) {
        Long borrow = 0L;
        List<History> histories1 = History.find("hospital.id=?1 AND user_id=?2", hospital_id, id).fetch();
        for (History history : histories1) {
            if (history.payment > 0)
                borrow += history.payment;
        }
        return borrow;
    }

    public Long getAge() {
        if (this.birthDate == null)
            return 0L;
        Long days = Functions.getDateDiff(this.birthDate, new Date(), TimeUnit.DAYS) + 1L;
        return days / 360 + 1;
    }

}
