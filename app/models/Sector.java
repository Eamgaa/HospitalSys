package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 7/15/18.
 */

@Entity(name = "tb_sector")
public class Sector extends Model {

    @Required
    public String name;

    @CRUD.Hidden
    @ManyToOne
    public Hospital hospital;

    @CRUD.Hidden
    @OneToMany(mappedBy = "sector", cascade = CascadeType.ALL)
    public List<SectorUserRel> sectorUserRels;

    public String toString() {
        return name;
    }

    public boolean checkDoctor(Long id) {
        for (SectorUserRel usr_rel: sectorUserRels) {
            if (usr_rel.user.id == id) return  true;
        }
        return  false;
    }
}
