package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity(name = "tb_daily_nursing_qa")
public class DailyNursingQA extends Model {


    @ManyToOne
    public DailyNursing dailyNursing;

    @ManyToOne
    public DailyNursingType dailyNursingType;

    @ManyToOne
    public DailyNursingAnswer dailyNursingAnswer;

}
