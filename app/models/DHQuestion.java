package models;

import controllers.CRUD;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by user on 1/17/2019.
 */
@Entity(name = "tb_dh_question")
public class DHQuestion extends Model {


    @ManyToOne
    public DHQuestionType questionType;

    public String name;

    public String answer;

    //нэмэлт хариултын нөхцөл
    public String answerCase;

    public String extraAnswer;

    @CRUD.Hidden
    @OneToMany(mappedBy = "dhQuestion", cascade = CascadeType.ALL)
    public List<DHAnswer> dhAnswers;

    public String[] getAnswers() {
        return answer.split("\\|");
    }

    public String[] getExtraAnswers() {
        return extraAnswer.split("\\|");
    }

}
