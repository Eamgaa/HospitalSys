package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by mungu on 2018-07-31.
 */
@Entity(name = "tb_complaint")
public class Complaint extends Model {

    @Required
    public String name;

    @ManyToOne
    public Hospital hospital;

    public String toString() {
        return name;
    }

    @OneToMany(mappedBy = "complaint", cascade = CascadeType.ALL)
    public List<InspecComplaintRel> inspecComplaintRels;
}
