package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity(name = "tb_hospital")
public class Hospital extends Model {

    @Required
    public String name;

    @CRUD.Hidden
    @Required
    public String logo;

    @Required
    public String direction;

    @Required
    public String phone;

    public String phone2;

    public String email;

    @CRUD.Hidden
    public Boolean active;

    @CRUD.Hidden
    public Date regDate;

    public String longitude;

    public String latitude;

    @CRUD.Hidden
    @ManyToOne
    public Sum sumName;

    @CRUD.Hidden
    @ManyToOne
    public Aimag aimag;

    @CRUD.Hidden
    @ManyToOne
    public Bag bag;

    public String address;

    public int portNumber; // посАПИ ажиллах портын дугаар

    public String districtCode; // Татварын албаны орон нутгийн код

    public Date suffixResetDay;

    public Integer lastBillIdSuffix;

    public String insuranceUsername;// ЭМД-тай холбогдох нэр

    public String insurancePassword; // ЭМД-тай холбогдох нууц үг

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<User> users;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<Room> rooms;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<Sector> sectors;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<Complaint> complaints;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<HospitalDrug> drugs;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<Tariff> tariffs;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<Inspection> inspections;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<BedTreatment> bedTreatments;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<HospitalModule> hospitalModules;

    @CRUD.Hidden
    @OneToMany(mappedBy = "hospital", cascade = CascadeType.ALL)
    public List<HospitalDiagnose> hospitalDiagnoses;

    public String toString() {
        return name;
    }

    public String getAddress() {
        return this.aimag + ", " + this.sumName + ", " + this.bag + ", " + address;
    }

    public boolean checkModule(Integer value) {
        for (HospitalModule hos_mod : hospitalModules) {
            if (hos_mod.module.value == value) return true;
        }
        return false;
    }
}
