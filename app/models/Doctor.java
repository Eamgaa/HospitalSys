package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by mungu on 2018-07-26.
 */
@Entity(name = "tb_doctor")
public class Doctor extends Model {

    @Required
    public String certificateNo;

    @Required
    public String degree;

    @Required
    public String position;

    @Required
    public String direction;

    @Required
    @OneToOne
    public User user;

}
