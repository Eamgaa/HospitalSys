package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by user on 2/20/2019.
 */
@Entity(name = "tb_inspection_type")
public class InspectionType extends Model {

    public String name;

    @OneToMany(mappedBy = "inspectionType", cascade = CascadeType.ALL)
    public List<Inspection> inspections;

    public String toString() {
        return name;
    }
}
