package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_daily_nursing_type")
public class DailyNursingType extends Model {

    public Integer value;

    @Required
    public String name;

    public Boolean normal;

    @OneToMany(mappedBy = "parentType", cascade = CascadeType.ALL)
    public List<DailyNursingType> subTypes;

    @ManyToOne
    public DailyNursingType parentType;

    public String toString() {
        return name;
    }

    public Integer childCount(){
        Integer childCount = 0;
        if (subTypes.size() == 0) return 1;
        for (DailyNursingType rel: subTypes){
            childCount += rel.childCount();
        }
        return childCount;
    }
}
