package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 7/18/18.
 */
@Entity(name = "tb_bed")
public class Bed extends Model {

    @Required
    public String bedNum;

    @CRUD.Hidden
    @ManyToOne
    public Room room;

    @CRUD.Hidden
    @OneToMany(mappedBy = "bed", cascade = CascadeType.ALL)
    public List<BedTreatment> bedTreatments;

    public String toString() {
        return bedNum;
    }

    public boolean isBedFree(Long bt_id, Date start, Date end) {

        List<BedTreatment> treatments;
        if (bt_id == null) {
            treatments = BedTreatment.find("bed.id=?1", this.id).fetch();
        } else {
            treatments = BedTreatment.find("bed.id=?1 AND id!=?2", this.id, bt_id).fetch();
        }
        for (BedTreatment bt : treatments) {
            if ((start.after(bt.startDate) && start.before(bt.endDate)) ||
                    (end.after(bt.startDate) && end.before(bt.endDate)) || (start.before(bt.startDate) && end.after(bt.endDate))
                    || start.equals(bt.startDate) || end.equals(bt.endDate)) {
                return false;
            }
        }
        return true;
    }
}
