package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by suren on 7/9/18.
 */
@Entity(name = "tb_bag")
public class Bag extends Model {
    @Required
    public String name;

    public String latitude;

    public String longitude;

    @CRUD.Hidden
    @ManyToOne
    public Sum sumName;

    @OneToMany(mappedBy = "bag", cascade = CascadeType.ALL)
    public List<Hospital> hospitals;

    public String toString() {
        return name;
    }
}
