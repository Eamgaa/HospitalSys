package models;

import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 5/13/2019.
 */
@Entity(name = "tb_sur_ins_user_rel")
public class SurveyInsUserRel extends Model {

    public Date createdDate;

    @ManyToOne
    public Survey survey;

    @OneToOne(mappedBy = "surveyInsUserRel", cascade = CascadeType.ALL)
    public Inspection inspection;

    @ManyToOne
    public User owner;

    @OneToMany(mappedBy = "surveyInsUserRel", cascade = CascadeType.ALL)
    public List<SurveyInsUserRelAnswer> relAnswers;

    public String getUserAnswer(Long qId) {
        SurveyInsUserRelAnswer relAnswer = SurveyInsUserRelAnswer
                .find("questionRel.id=?1 AND surveyInsUserRel.id=?2", qId, this.id).first();
        return relAnswer == null ? null : relAnswer.answer;
    }

    public String getUserExtraAnswer(Long qId) {
        SurveyInsUserRelAnswer relAnswer = SurveyInsUserRelAnswer
                .find("questionRel.id=?1 AND surveyInsUserRel.id=?2", qId, this.id).first();
        return relAnswer == null ? null : relAnswer.extraAnswer;
    }

}
