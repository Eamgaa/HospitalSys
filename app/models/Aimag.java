package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 7/9/18.
 */

@Entity(name = "tb_aimag")
public class Aimag extends Model{
    @Required
    public String name;
    public int queue;

    @OneToMany(mappedBy = "aimag", cascade = CascadeType.ALL)
    public List<Sum> sumNames; //багтах сумууд

    public String toString() {
        return name;
    }
}
