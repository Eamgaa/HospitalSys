package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by suren on 7/16/18.
 */
@Entity(name = "tb_sector_user_rel")
public class SectorUserRel extends Model {

    @ManyToOne
    public Sector sector;

    @ManyToOne
    public User user;

    public String toString() {
        return user.toString();
    }
}
