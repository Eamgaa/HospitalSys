package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 8/31/18.
 */
@Entity(name = "tb_drug_dose")
public class DrugDose extends Model {
    @Required
    public String dose;

    @OneToMany(mappedBy = "dose", cascade = CascadeType.ALL)
    public List<Drug> drugs;

    public String toString(){
        return dose;
    }
}
