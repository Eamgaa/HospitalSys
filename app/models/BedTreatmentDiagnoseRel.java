package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by suren on 8/6/18.
 */
@Entity(name = "tb_bed_treatment_diagnose")
public class BedTreatmentDiagnoseRel extends Model{
    @ManyToOne
    public BedTreatment bedTreatment;

    @ManyToOne
    public Diagnose diagnose;
}
