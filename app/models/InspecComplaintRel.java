package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by mungu on 2018-08-01.
 */
@Entity(name = "tb_inspec_comp_rel")
public class InspecComplaintRel extends Model {

    @ManyToOne
    public Inspection inspection;

    @ManyToOne
    public Complaint complaint;
}
