package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;

@Entity(name = "tb_daily_nursing_answer")
public class DailyNursingAnswer extends Model {

    @Required
    public Integer category;

    @Required
    public String issue;

    public String toString() {
        return issue;
    }
}
