package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Created by user on 1/3/2019.
 */
@Entity(name = "tb_medication_date")
public class MedicationDate extends Model {

    @Required
    public Date date;

    @ManyToOne
    public TreatmentPlan treatmentPlan;

    public boolean morning;

    public boolean noon;

    public boolean evening;

}
