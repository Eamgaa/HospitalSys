package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Created by suren on 9/13/18.
 */
@Entity(name = "tb_user_used_drug_date")
public class UserUsedDrugDate extends Model {

    @Required
    public Date date;

    @ManyToOne
    public UserUsedDrug drug;

    public Boolean morning;

    public Boolean day;

    public Boolean evening;
}
