package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 9/13/18.
 * Эмчлүүлэгчийн хэрэглэсэн эм
 */
@Entity(name = "tb_user_user_drug")
public class UserUsedDrug extends Model {

    @ManyToOne
    public BedTreatment bedTreatment;

    @ManyToOne
    public TreatmentPlan treatmentPlan;

    @ManyToOne
    public HospitalDrug drug;

    public String drugName;

    public String dose;

    public String instruction;

    @OneToMany(mappedBy = "drug", cascade = CascadeType.ALL)
    public List<UserUsedDrugDate> dates;

    public Integer usedCount;

    public Long unitPrice;

    public Long Price;

    public String toString() {
        return drugName;
    }

    public Boolean checkMorning(Date date) {
        if (dates != null) {
            for (UserUsedDrugDate d : dates) {
                if (d.date != null && d.date.compareTo(date) == 0) {
                    return d.morning;
                }
            }
        }
        return false;
    }

    public Boolean checkDay(Date date) {
        if (dates != null) {
            for (UserUsedDrugDate d : dates) {
                if (d.date != null && d.date.compareTo(date) == 0) return d.day;
            }
        }
        return false;
    }

    public Boolean checkEvening(Date date) {
        if (dates != null) {
            for (UserUsedDrugDate d : dates) {
                if (d.date != null && d.date.compareTo(date) == 0) return d.evening;
            }
        }
        return false;
    }
}
