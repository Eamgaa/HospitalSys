package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by mungu on 2018-08-01.
 */
@Entity(name = "tb_repeat")
public class Repeat extends Model {

    @Required
    public String name;

    @CRUD.Hidden
    @ManyToOne
    public Hospital hospital;

    @OneToMany(mappedBy = "repeat", cascade = CascadeType.ALL)
    public List<InspectionPlan> inspectionPlans;

    @OneToMany(mappedBy = "repeat", cascade = CascadeType.ALL)
    public List<InspectionDrug> inspectionDrugs;

    @OneToMany(mappedBy = "repeat", cascade = CascadeType.ALL)
    public List<TreatmentPlan> treatmentPlans;

    public String toString() {
        return this.name;
    }

}
