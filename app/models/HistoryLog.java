package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by mungu on 2018-07-29.
 */
@Entity(name = "tb_history_log")
public class HistoryLog extends Model {

    public Date createDate;

    @OneToMany(mappedBy = "historyLog", cascade = CascadeType.ALL)
    public List<HistoryLogRel> historyLogRels;

    public Long payment = 0L;           //нийт төлөх

    public Long discount = 0L;          //хөнгөлөлт

    public Long discountVal = 0L;       //хөнгөлөлтийн утга

    public Long cashPay = 0L;           //бэлнээр төлсөн

    public Long cardPay = 0L;           //картаар төлсөн

    public Long borrow = 0L;            //үлдэгдэл

    public String customerNo;
    public String billIdSuffix;
    public String billSuffixId;

    public String billId;
    public Date date;

    @ManyToOne
    public User owner;

}
