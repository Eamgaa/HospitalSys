package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by mungu on 2018-07-29.
 */

@Entity(name = "tb_history")
public class History extends Model {

    @ManyToOne
    public User user;

    @ManyToOne
    public Hospital hospital;

    @OneToMany(mappedBy = "history", cascade = CascadeType.ALL)
    public List<HistoryLogRel> historyLogRels;

    @OneToMany(mappedBy = "history", cascade = CascadeType.ALL)
    public List<HistoryItem> historyItems;

    public int typeId;
    //1 = эмчийн үзлэг
    //2 = өдрийн эмчилгээ
    //3 = хэвтэн эмчлүүлэх
    //4 = хэвтэн эмчилгээ
    //5 = хэвтэн эмчилгээ эм бичих
    //6 = үзлэг эм бичих

    public Long pkid;
    //typeId = 1 bol pkid = inspection id эмчилгээ
    //typeId = 2 bol pkid = inspection id эм
    //typeId = 3 bol pkid = BedTreatment id
    //typeId = 4 bol pkid = BedTreatment plan id эмчилгээ
    //typeId = 5 bol pkid = BedTreatment plan id эм
    //typeId = 6 bol pkid = inspection plan id

    @ManyToOne
    public Inspection inspection;

    @ManyToOne
    public User owner;

    public Long discount = 0L;          //хөнгөлөлт

    public Long payment = 0L;            //төлөх дүн

    public Inspection getInspection() {
        return Inspection.findById(pkid);
    }

     public BedTreatment getBedTreatment() {
        return BedTreatment.findById(pkid);
    }

    public List<InspectionDrug> getDrugs() {
        return InspectionDrug.find("inspection.id = ?1 and shouldPay = true", inspection.id).fetch();
    }
}
