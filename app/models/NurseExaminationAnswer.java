package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;

/**
 * Created by suren on 9/20/18.
 */
@Entity(name = "tb_nurse_examination_answer")
public class NurseExaminationAnswer extends Model {

    @Required
    public Integer category;

    @Required
    public String issue;

    public String toString() {
        return issue;
    }
}
