package models;

import controllers.Functions;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Arrays;

/**
 * Created by user on 5/13/2019.
 */
@Entity(name = "tb_sur_ins_user_rel_answer")
public class SurveyInsUserRelAnswer extends Model {

    @ManyToOne
    public SurveyInsUserRel surveyInsUserRel;

    @ManyToOne
    public SurveyQuestionRel questionRel;

    public String answer;

    public String extraAnswer;

    public String getAnswer() {
        String[] arr = answer.split("\\|");
        return Functions.arrayToString(arr);
    }

    public String getExtraAnswer() {
        if (extraAnswer == null)
            return "";
        String[] arr = extraAnswer.split("\\|");
        return Functions.arrayToString(arr);
    }

}
