package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Created by user on 1/18/2019.
 */
@Entity(name = "tb_dh_answer")
public class DHAnswer extends Model {

    public String answer;

    public String extraAnswer;

    public String description;

    @ManyToOne
    public DHQuestion dhQuestion;

    @ManyToOne
    public DiseaseHistory diseaseHistory;
}
