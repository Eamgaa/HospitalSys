package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by user on 4/23/2019.
 */
@Entity(name = "tb_survey")
public class Survey extends Model {

    public String name;

    public Long value;            // Асуумжний дугаар /html дугаараар дуудаж харуулдаг/

    public String description;

//    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL)
//    public List<SurveyQuestionRel> surveyQuestionRels;

    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL)
    public List<SurveyQuestionType> surveyQuestionTypes;

    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL)
    public List<SurveyInsUserRel> surveyInsUserRels;
}
