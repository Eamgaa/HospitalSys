package models;

import controllers.CRUD;
import controllers.Functions;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by suren on 7/19/18.
 */
@Entity(name = "tb_bed_treatment")
public class BedTreatment extends Model {
    @CRUD.Hidden
    @Required
    @ManyToOne
    public User customer;

    @CRUD.Hidden
    @Required
    @ManyToOne
    public User doctor;

    @Required
    @ManyToOne
    public Bed bed;

    public String receiptNumber;

    public Date startDate;

    public Date endDate;
    public Date createdDate; //Хэзээ үүсгэсэн

    @CRUD.Hidden
    public Boolean active; //эмчилгээ явж байгаа эсэх

    @CRUD.Hidden
    public Boolean paid;//төлбөрөө төлсөн эсэх

    @CRUD.Hidden
    @ManyToOne
    public Hospital hospital;

    @CRUD.Hidden
    @OneToOne
    public DiseaseHistory diseaseHistory;

    @OneToMany(mappedBy = "bedTreatment", cascade = CascadeType.ALL)
    public List<TreatmentPlan> treatmentPlans;

    @OneToMany(mappedBy = "bedTreatment", cascade = CascadeType.ALL)
    public List<BedTreatmentDiagnoseRel> bedTreatmentDiagnoseRels;

    @OneToMany(mappedBy = "bedTreatment", cascade = CascadeType.ALL)
    public List<VitalIndex> vitalIndices;

    @OneToMany(mappedBy = "bedTreatment", cascade = CascadeType.ALL)
    public List<NurseExamination> nurseExaminations;

    @OneToMany(mappedBy = "bedTreatment", cascade = CascadeType.ALL)
    public List<DailyNursing> dailyNursings;

    @OneToMany(mappedBy = "bedTreatment", cascade = CascadeType.ALL)
    public List<BedTreatmentInspection> bedTreatmentInspections;

    @OneToMany(mappedBy = "bedTreatment", cascade = CascadeType.ALL)
    public List<UserUsedDrug> usedDrugs;

    public Long payment;

    @ManyToOne
    public Tariff bedTreatmentTariff;

    public String toString() {
        return customer.toString() + "(" + bed.room + "-" + bed + ")";
    }

    public boolean checkDiagnose(Long id) {
        for (BedTreatmentDiagnoseRel rel : this.bedTreatmentDiagnoseRels) {
            if (rel.diagnose.id.compareTo(id) == 0) return true;
        }
        return false;
    }

    public List<TreatmentPlan> getTreatmentPlans() {
        return TreatmentPlan.find("bedTreatment_id=?1 AND type=1", id).fetch();
    }

    public List<TreatmentPlan> getDrugPlans() {
        return TreatmentPlan.find("bedTreatment_id=?1 AND type=2", id).fetch();
    }

    public Long countTimes() {
        Long days = Functions.getDateDiff(startDate, endDate, TimeUnit.DAYS) + 1L;
        return days;
    }
}
