package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by user on 5/10/2019.
 */

@Entity(name = "tb_survey_question_type")
public class SurveyQuestionType extends Model {

    public String name;

    @ManyToOne
    public Survey survey;

    @OneToMany(mappedBy = "questionType", cascade = CascadeType.ALL)
    public List<SurveyQuestionRel> questionRels;

    public String toString() {
        return name;
    }
}
