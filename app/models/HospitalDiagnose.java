package models;

import controllers.CRUD;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by user on 1/31/2019.
 */
@Entity(name = "tb_hospital_diagnose")
public class HospitalDiagnose extends Model {

    @ManyToOne
    public Diagnose diagnose;

    @CRUD.Hidden
    @ManyToOne
    public Hospital hospital;

}
