package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Created by suren on 8/13/18.
 */
@Entity(name = "tb_nurse_note")
public class NurseNote extends Model {

    @Required
    @Lob
    public String note;

    @ManyToOne
    public User user;

    public Date date;

    @ManyToOne
    public InspectionPlan inspectionPlan;

    @ManyToOne
    public TreatmentPlan treatmentPlan;

    public String toString() {
        return note;
    }
}
