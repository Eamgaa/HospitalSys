package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 7/9/18.
 */
@Entity(name = "tb_sum")
public class Sum extends Model {
    @Required
    public String name;

    public String latitude;

    public String longitude;

    @CRUD.Hidden
    @ManyToOne
    public Aimag aimag;

    @OneToMany(mappedBy = "sumName", cascade = CascadeType.ALL)
    public List<Bag> bags;

    @OneToMany(mappedBy = "sumName", cascade = CascadeType.ALL)
    public List<Hospital> hospitals;


    public String toString() {
        return name;
    }
}
