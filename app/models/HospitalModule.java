package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by suren on 8/8/18.
 */
@Entity(name = "tb_hospital_module")
public class HospitalModule extends Model {

    @Required
    @ManyToOne
    public Hospital hospital;

    @Required
    @ManyToOne
    public Module module;

    public String toString() {
        return module.toString();
    }
}
