package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 7/30/18.
 */
@Entity(name = "tb_hospital_drug")
public class HospitalDrug extends Model {

    @Required
    @ManyToOne
    public Drug drug;

    public String code; // Ebarimt( барааны дотоод код)

    public String measureUnit; // Ebarimt( хэмжих нэгж)

    public Long unitPrice;

    @CRUD.Hidden
    @ManyToOne
    public Hospital hospital;

    public String toString() {
        return this.drug.toString();
    }

    @OneToMany(mappedBy = "drug", cascade = CascadeType.ALL)
    public List<InspectionDrug> inspectionDrugs;

    @OneToMany(mappedBy = "drug", cascade = CascadeType.ALL)
    public List<TreatmentPlan> treatmentPlans;
}
