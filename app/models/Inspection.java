package models;

import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by mungu on 2018-07-19.
 */
@Entity(name = "tb_inspection")
public class Inspection extends Model {

//    public Date createdDate;
//
//    public boolean deletePlug;

    public Date startDate;

    public Date endDate;

    @ManyToOne
    public Hospital hospital;

    @ManyToOne
    public User customer;

    @ManyToOne
    public Sector sector;

    @ManyToOne
    public User doctor;

    @ManyToOne
    public User owner;

    @ManyToOne
    public Tariff tariff;

    public Long totalPrice;

    public Date serviceDate;

    @Column(length = 65535)
    public String note;

    @Column(length = 65535)
    public String complaintNote;

    @Column(length = 65535)
    public String questionnaireNote;

    public String receiptNumber;

    public boolean deletePlug;

    @ManyToOne // үзлэгийн төрөл: анхан, давтан,
    public InspectionType inspectionType;

    @OneToMany(mappedBy = "inspection", cascade = CascadeType.ALL)
    public List<InspecDiagnoseRel> inspecDiagnoseRels;

    @OneToMany(mappedBy = "inspection", cascade = CascadeType.ALL)
    public List<InspecComplaintRel> inspecComplaintRels;

    @OneToMany(mappedBy = "inspection", cascade = CascadeType.ALL)
    public List<InspectionPlan> inspectionPlans;

    @OneToMany(mappedBy = "inspection", cascade = CascadeType.ALL)
    public List<InspectionDrug> inspectionDrugs;

    @OneToMany(mappedBy = "inspection", cascade = CascadeType.ALL)
    public List<History> histories;

    @OneToOne
    public SurveyInsUserRel surveyInsUserRel;

    public boolean checkDiagnose(Long id) {
        for (InspecDiagnoseRel rel : this.inspecDiagnoseRels) {
            if (rel.diagnose.id.compareTo(id) == 0) return true;
        }
        return false;
    }

    public boolean checkComplaint(Long id) {
        for (InspecComplaintRel rel : this.inspecComplaintRels) {
            if (rel.complaint.id.compareTo(id) == 0) return true;
        }
        return false;
    }

    public History getPlanHistory() {
        History history = History.find("typeId = 2 and pkid = " + this.id).first();
        return history;
    }

    public boolean isPaid() {
        History history = History.find("typeId = 6 and pkid = " + this.id).first();
        return history != null && history.historyLogRels != null && history.historyLogRels.size() > 0;
    }

    public String getDiagnoses() {
        String str = "";
        for (InspecDiagnoseRel rel : inspecDiagnoseRels)
            str += rel.diagnose.toString() + ", ";
        return str;
    }

    public String getComplaints() {
        String str = "";
        for (InspecComplaintRel rel : inspecComplaintRels)
            str += rel.complaint.toString() + ", ";
        return str;
    }
}
