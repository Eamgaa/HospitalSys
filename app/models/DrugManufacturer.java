package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 8/31/18.
 */
@Entity(name="tb_drug_manufacturer")
public class DrugManufacturer extends Model {

    @Required
    public String name;

    public String country;

    @CRUD.Hidden
    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL)
    public List<Drug> drugs;

    public String toString(){
        return name;
    }
}
