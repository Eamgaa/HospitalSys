package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity(name = "bed_treatment_inspection")
public class BedTreatmentInspection extends Model {

    @ManyToOne
    public BedTreatment bedTreatment;

    public Date date; // үзлэг хийсэн огноо

    @Lob
    public String note; // үзлэгийн тэмдэглэл

    @Lob
    public String instruction; //эмчилгээ, хоол, сувилгааны заалт
}
