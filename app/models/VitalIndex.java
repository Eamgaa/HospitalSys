package models;

import controllers.Consts;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;
/**
 * Эмчлүүлэгчийн амин үзүүлэлт
 * Created by suren on 9/17/18.
 */
@Entity(name = "tb_vital_index")
public class VitalIndex extends Model{

    @ManyToOne
    public BedTreatment bedTreatment;

    public Date date;

    public String dayPulses; // 06-18 цагийн хооронд пульс - цохилтын тоо

    public String nightPulses; // 18-06 цагийн хооронд пульс - цохилтын тоо

    public String dayRespirations; // 06-18 цагийн хооронд амьсгалын тоо

    public String nightRespirations; // 18-06 цагийн хооронд амьсгалын тоо

    public String dayTemperatures; // 06-18 цагийн хооронд биеийн халуун

    public String nightTemperatures; // 18-06 цагийн хооронд биеийн халуун

    public Long dayPressure;

    public Long nightPressure;

    public Integer mealCount;

    public Long bodyWeight;

    public Integer dayDung;

    public Integer nightDung;

    public Integer dayPiss;

    public Integer nightPiss;

    @ManyToOne
    public User morningNurse;

    @ManyToOne
    public User dayNurse;

    @ManyToOne
    public User eveningNurse;

    public String toString(){
        return Consts.dateFormat.format(date);
    }
}
