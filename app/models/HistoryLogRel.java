package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by suren on 8/22/18.
 */
@Entity(name = "tb_history_log_rel")
public class HistoryLogRel extends Model {
    @ManyToOne
    public History history;

    @ManyToOne
    public HistoryLog historyLog;
}
