package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 7/20/18.
 */
@Entity(name = "tb_tariff")
public class Tariff extends Model {

    @Required
    public Long price; //төлбөр

    // Ebarimtad heregtei

    public String code;
    public String measureUnit;

    @Required
    public String description; //тайлбар

    @CRUD.Hidden
    @Required
    public Integer tariffType; // 1-эмчилгээ, 2-үзлэг, 3-хэвтэн эмчлүүлэх

    @CRUD.Hidden
    @ManyToOne
    public Hospital hospital;

    @CRUD.Hidden
    @OneToMany(mappedBy = "tariff", cascade = CascadeType.ALL)
    public List<Inspection> inspections;

    @OneToMany(mappedBy = "tariff", cascade = CascadeType.ALL)
    public List<InspectionPlan> inspectionPlans;

    public String toString() {
        return description;
    }
}
