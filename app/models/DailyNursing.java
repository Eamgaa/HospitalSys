package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity(name = "tb_daily_nursing")
public class DailyNursing extends Model {

    @ManyToOne
    public BedTreatment bedTreatment;

    public Date date;

    @OneToMany(mappedBy = "dailyNursing", cascade = CascadeType.ALL)
    public List<DailyNursingQA> qaList;

    public Boolean selected(Long tid, Long aid) {
        for (DailyNursingQA qa: qaList) {
            if (qa.dailyNursingType.id == tid && qa.dailyNursingAnswer.id == aid) {
                return true;
            }
        }
        return false;
    }
}
