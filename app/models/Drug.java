package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
//import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 8/21/18.
 */
@Entity(name = "tb_drug")
public class Drug extends Model {


    public String registrationNumber; // Бүртгэлийн дугаар

    public String sellName; // Худалдааны нэр

    public String internationalName;    // Олон улсын нэр

    @ManyToOne
    public DrugDose dose;     // Тун


    @ManyToOne
    public DrugForm drugForm; // Эмийн хэлбэр

    public Boolean withRecipe;// Жортой олгох эсэх
    // 0 - жоргүй
    // 1 - жороор
    // 2 - Эмнэлгийн нөхцөлд хэрэглэнэ

    public String no; // №


    @ManyToOne
    public DrugManufacturer manufacturer;  // үйлдвэрлэгч

    public Date validDate;      // хүчинтэй хугацаа

    public Boolean valid;       // хүчинтэй эсэх
    // 0 - хугацаа дууссан
    // 1 - бүртгэсэн
    // 2 - бүртгэлээс хасагдсан

    public String image;      // зураг

    public String instruction; // заавар

    public String toString() {
        return sellName + " " + dose + " " + drugForm;
    }
}
