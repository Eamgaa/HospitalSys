package models;

import controllers.Functions;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by mungu on 2018-08-02.
 */
@Entity(name = "tb_inspection_drug")
public class InspectionDrug extends Model {

    @ManyToOne
    public Inspection inspection;

    @ManyToOne
    public HospitalDrug drug;

    @ManyToOne
    public Repeat repeat;

    public Date startDate;

    public Date endDate;

    public Long price;

    public boolean shouldPay;

    public String instruction;

    public Long countTimes() {
        Long days = Functions.getDateDiff(startDate, endDate, TimeUnit.DAYS) + 1L;
        Scanner in = new Scanner(repeat.toString()).useDelimiter("[^0-9]+");
        int rep = in.nextInt();
        return days * rep;
    }
}
