package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by mungu on 2018-07-31.
 */
//онош
@Entity(name = "tb_diagnose")
public class Diagnose extends Model {

    @Required
    public String name;

    @Column(length = 65535)
    public String mnDescription;

    @Column(length = 65535)
    public String enDescription;

    public String toString() {
        return name;
    }

    @OneToMany(mappedBy = "diagnose", cascade = CascadeType.ALL)
    public List<InspecDiagnoseRel> inspecDiagnoseRels;

    @OneToMany(mappedBy = "diagnose", cascade = CascadeType.ALL)
    public List<BedTreatmentDiagnoseRel> bedTreatmentDiagnoseRels;

    @OneToMany(mappedBy = "diagnose", cascade = CascadeType.ALL)
    public List<HospitalDiagnose> hospitalDiagnoses;
}
