package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by suren on 9/17/18.
 */
@Entity(name = "tb_history_item")
public class HistoryItem extends Model {

    @ManyToOne
    public History history;

    @ManyToOne
    public HospitalDrug drug; // drug

    @ManyToOne
    public Tariff tariff; // bedTreatment, inspection, treatmentPlan

    public String code;

    public String name;

    public String measureUnit;

    public String unitPrice;

    public String qty;

    public String totalAmount;

    public String barCode;

    public Boolean paid;

    public String toString() {
        return name;
    }
}
