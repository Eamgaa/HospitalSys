package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 8/31/18.
 */
@Entity(name = "tb_drug_form")
public class DrugForm extends Model {
    @Required
    public String drugForm;

    @OneToMany(mappedBy = "drugForm", cascade = CascadeType.ALL)
    public List<Drug> drugs;

    public String toString(){
        return drugForm;
    }
}
