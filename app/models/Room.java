package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 7/16/18.
 */
@Entity(name = "tb_room")
public class Room extends Model {

    @Required
    public String roomNum;

    @CRUD.Hidden
    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    public List<Bed> beds;

    @CRUD.Hidden
    @ManyToOne
    public Hospital hospital;

    public String toString() {
        return roomNum;
    }

    public Boolean isFull(Date startDate, Date endDate) {
        for (Bed bed: beds) {
            List<BedTreatment> treatments = BedTreatment.find("bed.id=?1 and endDate<=?2 and startDate>=?3", bed.id, startDate, endDate).fetch();
            if (treatments.size()==0) {
                return false;
            }
        }
        return true;
    }
}
