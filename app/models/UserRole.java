package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;

@Entity(name = "tb_user_role")
public class UserRole extends Model {

    @Required
    public String role;

    public String description;

    public Integer queue;

    public String toString() {
        return this.description;
    }
}
