package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity(name = "tb_nurse_examination_qa")
public class NurseExaminationQA extends Model {

    @ManyToOne
    public NurseExamination nurseExamination;

    @ManyToOne
    public NurseExaminationType examinationType;

    @ManyToOne
    public NurseExaminationAnswer examinationAnswer;
}
