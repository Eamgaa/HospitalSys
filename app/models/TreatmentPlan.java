package models;

import controllers.Functions;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by suren on 7/30/18.
 */
@Entity(name = "tb_treatment_plan")
public class TreatmentPlan extends Model {

    @ManyToOne
    public BedTreatment bedTreatment;

    public Date startDate;

    public Date endDate;

    @ManyToOne
    public Tariff tariff;

    public Long price;

    public Long unitPrice;

    @ManyToOne
    public Repeat repeat;

    public int type; // 1 - Эмчилгээ 2- Эм

    public Boolean drugSell; // Эм эмнэлэгээс зарж байгаа бол тооцоонд оруулах эсэх

    @ManyToOne
    public HospitalDrug drug;

    @OneToMany
    public List<NurseNote> nurseNotes;

    @OneToMany(mappedBy = "treatmentPlan", cascade = CascadeType.ALL)
    public List<MedicationDate> medicationDates;  //Эмийн тэмдэглэл

    public Long countTimes() {
        Long days = Functions.getDateDiff(startDate, endDate, TimeUnit.DAYS) + 1L;
        Scanner in = new Scanner(repeat.toString()).useDelimiter("[^0-9]+");
        int rep = in.nextInt();
        return days * rep;
    }

    public Boolean checkMorning(Date date) {
        if (medicationDates != null) {
            for (MedicationDate d : medicationDates) {
                if (d.date != null && d.date.compareTo(date) == 0) {
                    return d.morning;
                }
            }
        }
        return false;
    }

    public Boolean checkNoon(Date date) {
        if (medicationDates != null) {
            for (MedicationDate d : medicationDates) {
                if (d.date != null && d.date.compareTo(date) == 0)
                    return d.noon;
            }
        }
        return false;
    }

    public Boolean checkEvening(Date date) {
        if (medicationDates != null) {
            for (MedicationDate d : medicationDates) {
                if (d.date != null && d.date.compareTo(date) == 0)
                    return d.evening;
            }
        }
        return false;
    }

    public boolean isTreatmentDate (Date date) {
        return Functions.dateToDate(startDate, endDate).contains(date);
    }
}
