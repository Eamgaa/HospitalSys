package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by suren on 9/20/18.
 */
@Entity(name = "tb_nurse_examination")
public class NurseExamination extends Model {

    @ManyToOne
    public BedTreatment bedTreatment;

    public Date date;

    @OneToMany(mappedBy = "nurseExamination", cascade = CascadeType.ALL)
    public List<NurseExaminationQA> qaList;

    public Boolean selected(Long tid, Long aid) {
        for (NurseExaminationQA qa: qaList) {
            if (qa.examinationType.id == tid && qa.examinationAnswer.id == aid) {
                return true;
            }
        }
        return false;
    }
}
