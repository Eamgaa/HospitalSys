package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by suren on 8/8/18.
 */
@Entity(name = "tb_module")
public class Module extends Model {

    @Required
    public int value;

    @Required
    public String name;

    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL)
    public List<HospitalModule> hospitalModules;

    public String toString() {
        return name;
    }
}
